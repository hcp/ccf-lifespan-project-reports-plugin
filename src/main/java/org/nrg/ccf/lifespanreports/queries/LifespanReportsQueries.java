package org.nrg.ccf.lifespanreports.queries;

public class LifespanReportsQueries {
	
	final static public String WHERE_REPLACE = "$$$WHERE$$$"; 
	
	final static public String MR_SCAN_QUERY = 
			"SELECT s.label  AS subj_label,s.id AS subj_id,meta.age AS subj_age,meta.gender AS subj_gender,meta.handedness AS subj_handedness, " +
			"       se.project AS project,se.id AS exp_id,se.label AS exp_label,se.date AS exp_date,se.time AS exp_time,se.note AS exp_note, " +
			"       isc.id AS scan_id, " +
			"       CASE WHEN isc.id ~ '^[0-9]+$' THEN to_number(isc.id,'99999') ELSE NULL END AS num_scan_id," +
			"		isc.type AS scan_type,isc.series_description AS series_description,isc.quality AS scan_quality, " +
			"       isc.scanner AS scan_scanner,isc.operator AS scan_operator,isc.note AS scan_note  " +
			"	FROM " +
			"		xnat_subjectdata s " +
			"		LEFT JOIN  " +
			"		xnat_demographicdata meta " +
			"		ON s.demographics_xnat_abstractdemographicdata_id=meta.xnat_abstractdemographicdata_id " +
			"		INNER JOIN     " +
			"		xnat_subjectassessordata sa " +
			"		ON s.id=sa.subject_id " +
			"		INNER JOIN " +
			"		xnat_experimentdata se " +
			"		ON sa.id=se.id " +
			"		INNER JOIN " +
			"		xnat_imagesessiondata ise " +
			"		ON se.id=ise.id " +
			"		INNER JOIN " +
			"		xnat_mrsessiondata mrse " +
			"		ON se.id=mrse.id " +
			"		INNER JOIN " +
			"		xnat_imagescandata isc " +
			"		ON se.id=isc.image_session_id " +
			"		INNER JOIN " +
			"		xnat_mrscandata mrsc " +
			"		ON isc.xnat_imagescandata_id=mrsc.xnat_imagescandata_id " +
			"	" + WHERE_REPLACE + " " +
			"	ORDER BY exp_label,num_scan_id,scan_id " +
			"; "
	;
	
	final static public String ALL_SCAN_QUERY = 
			"SELECT se.id AS imageSessionId, " +
			"   CASE WHEN isc.id ~ '^[0-9]+$' THEN to_number(isc.id,'99999') ELSE NULL END AS numScanId," +
			"   isc.id AS id, isc.type AS type,isc.series_description AS seriesDescription," +
			"	isc.quality AS quality, isc.frames AS frames,isc.note AS note " +
			"      FROM" +
			"         xnat_experimentdata se" +
			"         INNER JOIN" +
			"         xnat_imagesessiondata ise" +
			"         ON se.id=ise.id" +
			"         INNER JOIN" +
			"         xnat_mrsessiondata mrse" +
			"         ON se.id=mrse.id" +
			"         INNER JOIN" +
			"         xnat_imagescandata isc" +
			"         ON se.id=isc.image_session_id" +
			"         INNER JOIN" +
			"         xnat_mrscandata mrsc" +
			"         ON isc.xnat_imagescandata_id=mrsc.xnat_imagescandata_id" +
			"	" + WHERE_REPLACE + " " +
			"     ORDER by imageSessionId,numScanId,id" +			
			"; "
	;
	
	final static public String SUBJECT_INFO_QUERY = 
			"SELECT s.id as id,s.label as label,d.age as age " +
			"  FROM" +
			"	xnat_subjectdata s" +
			"	LEFT JOIN" +
			"	xnat_demographicdata d" +
			"    ON s.demographics_xnat_abstractdemographicdata_id=d.xnat_abstractdemographicdata_id" +
			" " + WHERE_REPLACE + " " +
			" ORDER by label" +
			"; "
	;
	
	final static public String QC_STATS_QUERY = 
			"SELECT exp.id as expId,qcsc.id as scanId,stat.name as statName,stat.additionalstatistics as statValue" +
			"  FROM xnat_experimentdata exp" +
			"       INNER JOIN" +
			"       xnat_imageassessordata ia" +
			"       ON exp.id=ia.imagesession_id" +
			"       INNER JOIN" +
			"       xnat_qcassessmentdata qc" +
			"       ON ia.id=qc.id" +
			"       INNER JOIN" +
			"       xnat_qcassessmentdata_scan qcsc" +
			"       ON qc.id=qcsc.scans_scan_xnat_qcassessmentdat_id" +
			"       INNER JOIN" +
			"       xnat_abstractstatistics as abstat" +
			"       ON qcsc.scanstatistics_xnat_abstractstatistics_id=abstat.xnat_abstractstatistics_id" +
			"       INNER JOIN" +
			"       xnat_statisticsdata_additionalstatistics stat" +
			"       ON abstat.xnat_abstractstatistics_id=stat.xnat_statisticsdata_xnat_abstractstatistics_id" +
			" " + WHERE_REPLACE + " " +
			"; "
	;
			
}
