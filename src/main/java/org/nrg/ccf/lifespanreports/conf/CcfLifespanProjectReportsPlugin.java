package org.nrg.ccf.lifespanreports.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfLifespanProjectReportsPlugin",
			name = "CCF Lifespan Project Reports Plugin",
			log4jPropertiesFile="/META-INF/resources/lsReportsLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.lifespanreports.conf",
		"org.nrg.ccf.lifespanreports.components",
		"org.nrg.ccf.lifespanreports.scheduler",
		"org.nrg.ccf.lifespanreports.xapi"
	})
public class CcfLifespanProjectReportsPlugin {
	
	public static Logger logger = Logger.getLogger(CcfLifespanProjectReportsPlugin.class);

	public CcfLifespanProjectReportsPlugin() {
		logger.info("Configuring the CCF Lifespan Project Reports Plugin.");
	}
	
}
