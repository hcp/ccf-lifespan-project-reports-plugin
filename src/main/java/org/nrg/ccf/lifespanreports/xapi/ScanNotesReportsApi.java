package org.nrg.ccf.lifespanreports.xapi;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.lifespanreports.utils.ScanNotesReportUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.exception.InvalidPermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "LifeSpan Reports API")
public class ScanNotesReportsApi extends AbstractXapiRestController {

	private JdbcTemplate _jdbcTemplate;

	@Autowired
	protected ScanNotesReportsApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, JdbcTemplate jdbcTemplate) {
		super(userManagementService, roleHolder);
		_jdbcTemplate = jdbcTemplate;
	}
	
	@ApiOperation(value = "Gets LifeSpan Scan Notes Report Data Table", notes = "Returns LifeSpan Scan Notes Report Data Table.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/scanNotesReports/project/{projectId}/dataTable"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getDataTable(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		List<Map<String, String>> records;
		try {
			records = ScanNotesReportUtils.getDataTable(projectId, getSessionUser(), _jdbcTemplate);
			return new ResponseEntity<>(records,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			records = null;
			return new ResponseEntity<>(records,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
