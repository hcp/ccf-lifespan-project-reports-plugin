package org.nrg.ccf.lifespanreports.xapi;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.utils.PojoUtils;
import org.nrg.ccf.commons.utilities.exception.CcfProjectNotSupportedException;
import org.nrg.ccf.lifespanreports.components.QcTables;
import org.nrg.ccf.lifespanreports.pojos.LifespanCompletionReportRow;
import org.nrg.ccf.lifespanreports.pojos.interfce.LifespanCompletionReportRowI;
import org.nrg.ccf.lifespanreports.utils.LifespanCompletionReportUtils;
import org.nrg.ccf.lifespanreports.utils.LifespanReportUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.exception.InvalidPermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "LifeSpan Reports API")
public class LifespanReportsApi extends AbstractXapiRestController {

	@Autowired
	protected LifespanReportsApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	@ApiOperation(value = "Gets LifeSpan QC Subject and Session Counts Table", notes = "Returns LifeSpan QC Report Data Table.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/subjectSessionCountsTable"}, restrictTo=AccessLevel.Authenticated,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getSubjectSessionCounts() throws NrgServiceException {
		final List<Map<String,String>> records = LifespanReportUtils.getSubjectSessionCounts(getSessionUser());
		if (records.size()>=1) {
			return new ResponseEntity<>(records,HttpStatus.OK);
		} else {
			return new ResponseEntity<>(records,HttpStatus.FORBIDDEN);
		}
	}
	
	@ApiOperation(value = "Gets LifeSpan QC Report Summary and Data Tables", notes = "Returns LifeSpan QC Report Summary and Data Tables.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/project/{projectId}/qcTables"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<QcTables> getQcTables(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		try {
			if (!projectId.matches("CCF_HC[AD]_ITK")) {
				throw new InvalidPermissionException("ERROR:  This report is only available for LifeSpan HCA/HCD Intake projects.");
			}
			final QcTables qcTables = LifespanReportUtils.getQcTables(projectId, getSessionUser());
			return new ResponseEntity<>(qcTables,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}
	
	@ApiOperation(value = "Gets LifeSpan QC Report Summary Table", notes = "Returns LifeSpan QC Report Summary Table.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/project/{projectId}/qcSummaryTable"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getQcSummaryTable(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		try {
			if (!projectId.matches("CCF_HC[AD]_ITK")) {
				throw new InvalidPermissionException("ERROR:  This report is only available for LifeSpan HCA/HCD Intake projects.");
			}
			final List<Map<String,String>> records = LifespanReportUtils.getQcSummaryTable(projectId, getSessionUser(), null);
			return new ResponseEntity<>(records,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}
	
	@ApiOperation(value = "Gets LifeSpan QC Report Data Table", notes = "Returns LifeSpan QC Report Data Table.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/project/{projectId}/qcDataTable"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getQcDataTable(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		try {
			final List<Map<String,String>> records = LifespanReportUtils.getQcDataTable(projectId, getSessionUser());
			return new ResponseEntity<>(records,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}
	
	@ApiOperation(value = "Gets LifeSpan Completion Report Data Table", notes = "Returns LifeSpan Completion Report Data Table.",
			responseContainer = "List", response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/project/{projectId}/completionReport"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<LifespanCompletionReportRowI>> getCompletionReportDataTable(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		try {
			final List<LifespanCompletionReportRowI> records = LifespanCompletionReportUtils.getCompletionReport(projectId, getSessionUser());
			return new ResponseEntity<>(records,HttpStatus.OK);
		} catch (CcfProjectNotSupportedException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Gets LifeSpan Completion Report Csv", notes = "Returns LifeSpan Completion Report Data Table.",
			responseContainer = "List", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/lifespanReports/project/{projectId}/completionReportCsv"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getCompletionReportCsv(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		try {
			final List<LifespanCompletionReportRowI> records = LifespanCompletionReportUtils.getCompletionReport(projectId, getSessionUser());
			return new ResponseEntity<>(PojoUtils.pojoListToCsvStr(records),HttpStatus.OK);
		} catch (CcfProjectNotSupportedException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

}
