package org.nrg.ccf.lifespanreports.xapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.lifespanreports.utils.ScanQualityReportUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.exception.InvalidPermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "LifeSpan Reports API")
public class ScanQualityReportsApi extends AbstractXapiRestController {

	private JdbcTemplate _jdbcTemplate;

	@Autowired
	protected ScanQualityReportsApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, JdbcTemplate jdbcTemplate) {
		super(userManagementService, roleHolder);
		_jdbcTemplate = jdbcTemplate;
	}
	
	@ApiOperation(value = "Gets Scan Quality Report Summary Table", notes = "Returns LifeSpan Scan Quality Report Data Table.",
			responseContainer = "Map", response = List.class)
	//@ApiOperation(value = "Gets Scan Quality Report Summary Table", notes = "Returns LifeSpan Scan Quality Report Data Table.",
	//		responseContainer = "Map", response = TreeBasedTable.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/scanQualityReports/project/{projectId}/summaryTables"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    //public ResponseEntity<Map<String,TreeBasedTable<String, String, ?>>> getSummaryTables(@PathVariable("projectId") final String projectId) throws NrgServiceException {
    public ResponseEntity<Map<String, List<Map<String,String>>>> getSummaryTables(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		Map<String, List<Map<String,String>>> dataMap;
		try {
			dataMap = ScanQualityReportUtils.getSummaryTables(projectId, getSessionUser(), _jdbcTemplate);
			return new ResponseEntity<>(dataMap,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			dataMap = new HashMap<>();
			return new ResponseEntity<>(dataMap,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    /*
    public ResponseEntity<String> getSummaryTables(@PathVariable("projectId") final String projectId) throws NrgServiceException {
		Map<String, TreeBasedTable<String, String, ?>> records;
		try {
			records = ScanQualityReportUtils.getSummaryTables(projectId, getSessionUser(), _dataCache);
			return new ResponseEntity<>(_gson.toJson(records),HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			records = null;
			return new ResponseEntity<>("",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    */
	
	@ApiOperation(value = "Gets LifeSpan Scan Quality Report Data Table", notes = "Returns LifeSpan Scan Quality Report Data Table.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/scanQualityReports/project/{projectId}/dataTable"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getDataTable(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		List<Map<String, String>> records;
		try {
			records = ScanQualityReportUtils.getDataTable(projectId, getSessionUser(), _jdbcTemplate);
			return new ResponseEntity<>(records,HttpStatus.OK);
		} catch (InvalidPermissionException e) {
			records = null;
			return new ResponseEntity<>(records,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
