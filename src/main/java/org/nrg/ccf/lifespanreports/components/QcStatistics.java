package org.nrg.ccf.lifespanreports.components;

import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;

public class QcStatistics {
	
	private String expId;
	private String scanId;
	private Double absMeanRms;
	private Double relMeanRms;
	
	public QcStatistics(XnatMrsessiondata exp, ScanInfo scan) {
		this.expId = exp.getId();
		this.scanId = scan.getId();
	}

	public String getExpId() {
		return expId;
	}
	
	public String getScanId() {
		return scanId;
	}
	
	public Double getAbsMeanRms() {
		return absMeanRms;
	}
	
	public void setAbsMeanRms(Double absMeanRms) {
		this.absMeanRms = absMeanRms;
	}
	
	public Double getRelMeanRms() {
		return relMeanRms;
	}
	
	public void setRelMeanRms(Double relMeanRms) {
		this.relMeanRms = relMeanRms;
	}
	
}
