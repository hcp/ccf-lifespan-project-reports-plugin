package org.nrg.ccf.lifespanreports.components;

public class QcStat {
	
	private String expId;
	private String scanId;
	private String statName;
	private Double statValue;
	
	public String getExpId() {
		return expId;
	}
	
	public void setExpId(String expId) {
		this.expId = expId;
	}
	
	public String getScanId() {
		return scanId;
	}
	
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}

	public String getStatName() {
		return statName;
	}

	public void setStatName(String statName) {
		this.statName = statName;
	}

	public Double getStatValue() {
		return statValue;
	}

	public void setStatValue(Double statValue) {
		this.statValue = statValue;
	}
	
	
}
