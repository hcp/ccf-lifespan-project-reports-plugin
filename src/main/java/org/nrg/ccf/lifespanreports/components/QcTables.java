package org.nrg.ccf.lifespanreports.components;

import java.util.List;
import java.util.Map;

public class QcTables {

	private List<Map<String, String>> _qcDataTable;
	private List<Map<String, String>> _qcSummaryTable;

	public List<Map<String, String>> getQcDataTable() {
		return _qcDataTable;
	}

	public void setQcDataTable(List<Map<String, String>> qcDataTable) {
		_qcDataTable = qcDataTable;
		
	}

	public List<Map<String, String>> getQcSummaryTable() {
		return _qcSummaryTable;
	}

	public void setQcSummaryTable(List<Map<String, String>> qcSummaryTable) {
		_qcSummaryTable = qcSummaryTable;
	}

}
