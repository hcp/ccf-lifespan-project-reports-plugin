package org.nrg.ccf.lifespanreports.components;

public class ScanInfo {
	
	private String imageSessionId;
	private Integer numId;
	private String id;
	private String type;
	private String seriesDescription;
	private String quality;
	private Integer frames;
	private String note;

	public String getImageSessionId() {
		return imageSessionId;
	}

	public void setImageSessionId(String imageSessionId) {
		this.imageSessionId = imageSessionId;
	}

	public Integer getNumId() {
		return numId;
	}

	public void setNumId(Integer numId) {
		this.numId = numId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSeriesDescription() {
		return seriesDescription;
	}

	public void setSeriesDescription(String seriesDescription) {
		this.seriesDescription = seriesDescription;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Integer getFrames() {
		return frames;
	}

	public void setFrames(Integer frames) {
		this.frames = frames;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
