package org.nrg.ccf.lifespanreports.pojos.interfce;

public interface LifespanCompletionReportRowI {
	
	public String getSrc_subject_id();
	public String getSubjectkey();
	public String getInterview_date();
	public String getInterview_age();
	public String getSex();
	public Integer getFull_MR_Compl();
	
}
