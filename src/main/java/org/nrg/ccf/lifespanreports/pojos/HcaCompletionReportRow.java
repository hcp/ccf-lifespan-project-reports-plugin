package org.nrg.ccf.lifespanreports.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HcaCompletionReportRow extends LifespanCompletionReportRow {
	
	private Double tMRI_CARIT_PctCompl;
	private Double tMRI_FACENAME_PctCompl;
	private Double tMRI_VISMOTOR_PctCompl;
	private Integer HiResHpT2_Count;

}
