package org.nrg.ccf.lifespanreports.pojos;

import org.nrg.ccf.lifespanreports.pojos.interfce.LifespanCompletionReportRowI;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BandaCompletionReportRow implements LifespanCompletionReportRowI {
	
	private String src_subject_id;
	private String subjectkey;
	private String interview_date;
	private String interview_age;
	private String sex;
	private Integer Full_MR_Compl;
	private Integer T1_Count;
	private Integer T2_Count;
	private Integer RS_fMRI_Count;
	private Double RS_fMRI_PctCompl;
	private Integer dMRI_Compl;
	private Double dMRI_PctCompl;
	private Integer Full_Task_fMRI;
	private Double tMRI_PctCompl;
	private Double tMRI_GAMBLING_PctCompl;
	private Double tMRI_FACEMATCHING_PctCompl;
	private Double tMRI_CONFLICT1_PctCompl;
	private Double tMRI_CONFLICT2_PctCompl;
	private Double tMRI_CONFLICT_PctCompl;


}
