package org.nrg.ccf.lifespanreports.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HcdCompletionReportRow extends LifespanCompletionReportRow {
	
	private Double tMRI_CARIT_PctCompl;
	private Double tMRI_EMOTION_PctCompl;
	private Double tMRI_GUESSING_PctCompl;

}
