package org.nrg.ccf.lifespanreports.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nrg.ccf.commons.utilities.exception.CcfProjectNotSupportedException;
import org.nrg.ccf.lifespanreports.pojos.BandaCompletionReportRow;
import org.nrg.ccf.lifespanreports.pojos.HcaCompletionReportRow;
import org.nrg.ccf.lifespanreports.pojos.HcdCompletionReportRow;
import org.nrg.ccf.lifespanreports.pojos.LifespanCompletionReportRow;
import org.nrg.ccf.lifespanreports.pojos.interfce.LifespanCompletionReportRowI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

public class LifespanCompletionReportUtils {
	
	public static final String[] completion_report_projects = new String[] { "CCF_HCA_STG", "CCF_HCD_STG", "CCF_BANDA_STG" };

	public static List<LifespanCompletionReportRowI> getCompletionReport(String projectId, UserI sessionUser) throws CcfProjectNotSupportedException {
		
		
		if (!Arrays.asList(completion_report_projects).contains(projectId)) {
			throw new CcfProjectNotSupportedException("This project is not supported by this report");
		}
		return buildReport(projectId, sessionUser);
	}
	
	public static List<LifespanCompletionReportRowI> buildReport(String projectId, UserI sessionUser) throws CcfProjectNotSupportedException {
		
		//final JdbcTemplate jdbcTemplate = XDAT.getContextService().getBeanSafely(JdbcTemplate.class);
		//List<ScanInfo> scaninfo = LifespanReportUtils.getScanInfo(projectId, sessionUser, jdbcTemplate);
		
		if (!Arrays.asList(completion_report_projects).contains(projectId)) {
			throw new CcfProjectNotSupportedException("This project is not supported by this report");
		}
        final CriteriaCollection cc = new CriteriaCollection("OR");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final ArrayList<XnatMrsessiondata> mrSessions = XnatMrsessiondata.getXnatMrsessiondatasByField(cc, sessionUser, false);
		List<LifespanCompletionReportRowI> reportList = new ArrayList<>();
		for (XnatMrsessiondata mrSession : mrSessions) {
			if (projectId.equalsIgnoreCase("CCF_HCA_STG")) {
				reportList.add(buildHcaCompletionReportRow(mrSession));
			} else if (projectId.equalsIgnoreCase("CCF_HCD_STG")) {
				reportList.add(buildHcdCompletionReportRow(mrSession));
			} else if (projectId.equalsIgnoreCase("CCF_BANDA_STG")) {
				reportList.add(buildBandaCompletionReportRow(mrSession));
			}
		}
		reportList.sort((r1i, r2i) -> {
			int cmp = r1i.getSrc_subject_id().compareTo(r2i.getSrc_subject_id());
			if (cmp == 0) {
				final LifespanCompletionReportRow r1 = (r1i instanceof LifespanCompletionReportRow) ? (LifespanCompletionReportRow) r1i : null;
				final LifespanCompletionReportRow r2 = (r2i instanceof LifespanCompletionReportRow) ? (LifespanCompletionReportRow) r2i : null;
				if (r1 == null || r2 == null) {
					return 0;
				}
				cmp = r1.getVisit().compareTo(r2.getVisit());
			}
			return cmp;
		});
		return reportList;
	}

	private static LifespanCompletionReportRowI buildHcaCompletionReportRow(XnatMrsessiondata mrSession) {
		final HcaCompletionReportRow reportRow = new HcaCompletionReportRow();
		buildLifespanCommonMeasures(mrSession, reportRow);
		buildHcaMeasures(mrSession, reportRow);
		return reportRow;
	}

	private static LifespanCompletionReportRowI buildHcdCompletionReportRow(XnatMrsessiondata mrSession) {
		final HcdCompletionReportRow reportRow = new HcdCompletionReportRow();
		boolean isYounger = buildLifespanCommonMeasures(mrSession, reportRow);
		buildHcdMeasures(mrSession, reportRow, isYounger);
		return reportRow;
	}

	private static LifespanCompletionReportRowI buildBandaCompletionReportRow(XnatMrsessiondata mrSession) {
		final BandaCompletionReportRow reportRow = new BandaCompletionReportRow();
		buildBandaMeasures(mrSession, reportRow);
		return reportRow;
	}

	private static boolean buildLifespanCommonMeasures(XnatMrsessiondata mrSession, LifespanCompletionReportRow reportRow) {
		
		final String sessionLabel = mrSession.getLabel();
		final XnatSubjectdata subject = mrSession.getSubjectData();
		reportRow.setSrc_subject_id(subject.getLabel());
		reportRow.setSubjectkey(getSubjectKey(subject));
		reportRow.setSex(getSubjectGender(subject));
		reportRow.setInterview_date(getInterviewDate(mrSession));
		final String interviewAge = getInterviewAge(mrSession);
		reportRow.setInterview_age(interviewAge);
		reportRow.setVisit(getVisitFromSessionLabel(sessionLabel));
		int T1_Count = 0;
		int T2_Count = 0;
		int RS_fMRI_Count = 0;
		double RS_fMRI_PctCompl = 0;
		double RS1_fMRI_PctCompl = 0;
		double RS2_fMRI_PctCompl = 0;
		int dMRI_Compl = 0;
		double dMRI_PctCompl = 0;
		double dMRI98_PctCompl = 0;
		double dMRI99_PctCompl = 0;
		int PCASL_Compl = 0;
		double PCASL_PctCompl = 0;
		boolean isYounger = false;
		boolean hasRest = false;
		for (final XnatImagescandataI scan : mrSession.getScans_scan()) {
			if (scan.getType().equals("T1w")) {
				T1_Count+=1;
			} else if (scan.getType().equals("T2w")) {
				T2_Count+=1;
			} else if (scan.getType().equals("rfMRI")) {
				hasRest = true;
				RS_fMRI_Count+=1;
				if ( scan.getSeriesDescription().contains("1a_") || 
					 scan.getSeriesDescription().contains("1b_") || 
					 scan.getSeriesDescription().contains("2a_") || 
					 scan.getSeriesDescription().contains("2b_")) {
					isYounger = true;
				}
				if (scan.getSeriesDescription().contains("REST1") && RS1_fMRI_PctCompl == 0) {
					RS1_fMRI_PctCompl+=(getPctPairComplete(scan));
				} else if (scan.getSeriesDescription().contains("REST2") && RS2_fMRI_PctCompl == 0) {
					RS2_fMRI_PctCompl+=(getPctPairComplete(scan));
				}
			} else if (scan.getType().equals("dMRI")) {
				if (scan.getSeriesDescription().contains("dir98") && dMRI98_PctCompl == 0) {
					dMRI98_PctCompl+=(getPctPairComplete(scan));
				} else if (scan.getSeriesDescription().contains("dir99") && dMRI99_PctCompl == 0) {
					dMRI99_PctCompl+=(getPctPairComplete(scan));
				}
				dMRI_PctCompl+=(getPctPairComplete(scan));
			} else if (scan.getType().equals("mbPCASLhr")) {
				PCASL_PctCompl = getPctComplete(scan);
				PCASL_Compl = (PCASL_PctCompl>=100) ? 1 : 0;
			}
		}
		if (!hasRest) {
			if (interviewAge != null && !interviewAge.trim().isEmpty() && Integer.valueOf(interviewAge)>0 &&
					Integer.valueOf(interviewAge)<96) {
				isYounger = true;
			}
		}
		RS_fMRI_PctCompl = (RS1_fMRI_PctCompl+RS2_fMRI_PctCompl)/2;
		dMRI_PctCompl = (dMRI98_PctCompl+dMRI99_PctCompl)/2;
		dMRI_Compl = (dMRI_PctCompl>=100) ? 1 : 0;
		reportRow.setT1_Count(T1_Count);
		reportRow.setT2_Count(T2_Count);
		reportRow.setRS_fMRI_Count(RS_fMRI_Count);
		reportRow.setRS_fMRI_PctCompl(RS_fMRI_PctCompl);
		reportRow.setDMRI_Compl(dMRI_Compl);
		reportRow.setDMRI_PctCompl(dMRI_PctCompl);
		reportRow.setPCASL_Compl(PCASL_Compl);
		reportRow.setPCASL_PctCompl(PCASL_PctCompl);
		return isYounger;
	}

	private static void buildHcaMeasures(XnatMrsessiondata mrSession, HcaCompletionReportRow reportRow) {
		int HiResHpT2_Count = 0;
		double tMRI_CARIT_PctCompl = 0;
		double tMRI_FACENAME_PctCompl = 0;
		double tMRI_VISMOTOR_PctCompl = 0;
		for (final XnatImagescandataI scan : mrSession.getScans_scan()) {
			if (scan.getType().equals("TSE_HiResHp")) {
				HiResHpT2_Count+=1;
			} else if (scan.getType().equals("tfMRI")) {
				if (scan.getSeriesDescription().contains("CARIT")) {
					tMRI_CARIT_PctCompl = getPctComplete(scan, 300);
				}else if (scan.getSeriesDescription().contains("FACENAME")) {
					tMRI_FACENAME_PctCompl = getPctComplete(scan, 345);
				}else if (scan.getSeriesDescription().contains("VISMOTOR")) {
					tMRI_VISMOTOR_PctCompl = getPctComplete(scan, 194);
				}
			}
		}
		reportRow.setHiResHpT2_Count(HiResHpT2_Count);
		reportRow.setTMRI_CARIT_PctCompl(tMRI_CARIT_PctCompl);
		reportRow.setTMRI_FACENAME_PctCompl(tMRI_FACENAME_PctCompl);
		reportRow.setTMRI_VISMOTOR_PctCompl(tMRI_VISMOTOR_PctCompl);
		reportRow.setTMRI_PctCompl((tMRI_CARIT_PctCompl+tMRI_FACENAME_PctCompl+tMRI_VISMOTOR_PctCompl)/3);
		reportRow.setFull_Task_fMRI((reportRow.getTMRI_PctCompl()>=100) ? 1 : 0);
		reportRow.setFull_MR_Compl( (
										reportRow.getT1_Count()>=1 && 
										reportRow.getT2_Count()>=1 && 
										reportRow.getRS_fMRI_PctCompl()>=100 && 
										reportRow.getDMRI_Compl()>=1 && 
										reportRow.getFull_Task_fMRI()>=1 && 
										reportRow.getPCASL_Compl()>=1 &&
										reportRow.getHiResHpT2_Count()>=1
									) ? 1 : 0);
	}

	private static void buildHcdMeasures(XnatMrsessiondata mrSession, HcdCompletionReportRow reportRow, boolean isYounger) {
		double tMRI_CARIT_PctCompl = 0;
		double tMRI_GUESSING_PctCompl = 0;
		double tMRI_EMOTION_PctCompl = 0;
		for (final XnatImagescandataI scan : mrSession.getScans_scan()) {
			 if (scan.getType().equals("tfMRI")) {
				if (!isYounger) {
					if (scan.getSeriesDescription().contains("CARIT") && tMRI_CARIT_PctCompl==0) {
						tMRI_CARIT_PctCompl += getPctPairComplete(scan);
					}else if (scan.getSeriesDescription().contains("GUESSING") && tMRI_GUESSING_PctCompl==0) {
						tMRI_GUESSING_PctCompl += getPctPairComplete(scan);
					}else if (scan.getSeriesDescription().contains("EMOTION")) {
						tMRI_EMOTION_PctCompl = getPctComplete(scan, 178);
					}
				} else {
					if (scan.getSeriesDescription().contains("CARIT") && tMRI_CARIT_PctCompl==0) {
						tMRI_CARIT_PctCompl = getPctComplete(scan, 300);
					}else if (scan.getSeriesDescription().contains("GUESSING") && tMRI_GUESSING_PctCompl==0) {
						tMRI_GUESSING_PctCompl = getPctComplete(scan, 280);
					}else if (scan.getSeriesDescription().contains("EMOTION")) {
						tMRI_EMOTION_PctCompl = getPctComplete(scan, 178);
					}
				}
			}
		}
		reportRow.setTMRI_CARIT_PctCompl(tMRI_CARIT_PctCompl);
		reportRow.setTMRI_GUESSING_PctCompl(tMRI_GUESSING_PctCompl);
		reportRow.setTMRI_EMOTION_PctCompl(tMRI_EMOTION_PctCompl);
		reportRow.setTMRI_PctCompl((tMRI_CARIT_PctCompl+tMRI_EMOTION_PctCompl+tMRI_GUESSING_PctCompl)/3);
		reportRow.setFull_Task_fMRI((reportRow.getTMRI_PctCompl()>=100) ? 1 : 0);
		reportRow.setFull_MR_Compl( (
										reportRow.getT1_Count()>=1 && 
										reportRow.getT2_Count()>=1 && 
										reportRow.getRS_fMRI_PctCompl()>=100 && 
										reportRow.getDMRI_Compl()>=1 && 
										reportRow.getFull_Task_fMRI()>=1 && 
										reportRow.getPCASL_Compl()>=1
									) ? 1 : 0);
	}

	private static void buildBandaMeasures(XnatMrsessiondata mrSession, BandaCompletionReportRow reportRow) {
		final XnatSubjectdata subject = mrSession.getSubjectData();
		reportRow.setSrc_subject_id(subject.getLabel());
		reportRow.setSubjectkey(getSubjectKey(subject));
		reportRow.setSex(getSubjectGender(subject));
		reportRow.setInterview_date(getInterviewDate(mrSession));
		final String interviewAge = getInterviewAge(mrSession);
		reportRow.setInterview_age(interviewAge);
		int T1_Count = 0;
		int T2_Count = 0;
		int RS_fMRI_Count = 0;
		double RS_fMRI_PctCompl = 0;
		double RS1_fMRI_PctCompl = 0;
		double RS2_fMRI_PctCompl = 0;
		int dMRI_Compl = 0;
		double dMRI_PctCompl = 0;
		double dMRI98_PctCompl = 0;
		double dMRI99_PctCompl = 0;
		for (final XnatImagescandataI scan : mrSession.getScans_scan()) {
			if (scan.getType().equals("T1w")) {
				T1_Count+=1;
			} else if (scan.getType().equals("T2w")) {
				T2_Count+=1;
			} else if (scan.getType().equals("rfMRI")) {
				RS_fMRI_Count+=1;
				if (scan.getSeriesDescription().contains("REST1") && RS1_fMRI_PctCompl == 0) {
					RS1_fMRI_PctCompl+=(getPctPairComplete(scan));
				} else if (scan.getSeriesDescription().contains("REST2") && RS2_fMRI_PctCompl == 0) {
					RS2_fMRI_PctCompl+=(getPctPairComplete(scan));
				}
			} else if (scan.getType().equals("dMRI")) {
				if (scan.getSeriesDescription().contains("dir98") && dMRI98_PctCompl == 0) {
					dMRI98_PctCompl+=(getPctPairComplete(scan));
				} else if (scan.getSeriesDescription().contains("dir99") && dMRI99_PctCompl == 0) {
					dMRI99_PctCompl+=(getPctPairComplete(scan));
				}
				dMRI_PctCompl+=(getPctPairComplete(scan));
			}
		}
		RS_fMRI_PctCompl = (RS1_fMRI_PctCompl+RS2_fMRI_PctCompl)/2;
		dMRI_PctCompl = (dMRI98_PctCompl+dMRI99_PctCompl)/2;
		dMRI_Compl = (dMRI_PctCompl>=100) ? 1 : 0;
		reportRow.setT1_Count(T1_Count);
		reportRow.setT2_Count(T2_Count);
		reportRow.setRS_fMRI_Count(RS_fMRI_Count);
		reportRow.setRS_fMRI_PctCompl(RS_fMRI_PctCompl);
		reportRow.setDMRI_Compl(dMRI_Compl);
		reportRow.setDMRI_PctCompl(dMRI_PctCompl);
		double tMRI_GAMBLING_PctCompl = 0;
		double tMRI_FACEMATCHING_PctCompl = 0;
		double tMRI_CONFLICT1_PctCompl = 0;
		double tMRI_CONFLICT2_PctCompl = 0;
		for (final XnatImagescandataI scan : mrSession.getScans_scan()) {
			if (scan.getType().equals("tfMRI")) {
				if (scan.getSeriesDescription().contains("GAMBLING") && tMRI_GAMBLING_PctCompl == 0) {
					tMRI_GAMBLING_PctCompl+=(getPctPairComplete(scan));
				}else if (scan.getSeriesDescription().contains("FACEMATCHING") && tMRI_FACEMATCHING_PctCompl == 0) {
					tMRI_FACEMATCHING_PctCompl+=(getPctPairComplete(scan));
					// BANDA changed their FACEMATCHING task as of BANDA017 to have more frames.  This was not
					// accounted for in session building.
					Integer subjno = Integer.valueOf(subject.getLabel().replace("BANDA", ""));
					if (subjno!=null && subjno>=17) {
						tMRI_FACEMATCHING_PctCompl=tMRI_FACEMATCHING_PctCompl*(328d/395d);
					}
				}else if (scan.getSeriesDescription().contains("CONFLICT1") && tMRI_CONFLICT1_PctCompl == 0) {
					tMRI_CONFLICT1_PctCompl+=(getPctPairComplete(scan));
				}else if (scan.getSeriesDescription().contains("CONFLICT2") && tMRI_CONFLICT2_PctCompl == 0) {
					tMRI_CONFLICT2_PctCompl+=(getPctPairComplete(scan));
				}
			}
		}
		reportRow.setTMRI_GAMBLING_PctCompl(tMRI_GAMBLING_PctCompl);
		reportRow.setTMRI_FACEMATCHING_PctCompl(tMRI_FACEMATCHING_PctCompl);
		reportRow.setTMRI_CONFLICT1_PctCompl(tMRI_CONFLICT1_PctCompl);
		reportRow.setTMRI_CONFLICT2_PctCompl(tMRI_CONFLICT2_PctCompl);
		reportRow.setTMRI_CONFLICT_PctCompl((tMRI_CONFLICT1_PctCompl+tMRI_CONFLICT2_PctCompl)/2);
		reportRow.setTMRI_PctCompl((tMRI_GAMBLING_PctCompl+tMRI_FACEMATCHING_PctCompl+tMRI_CONFLICT1_PctCompl+tMRI_CONFLICT2_PctCompl)/4);
		reportRow.setFull_Task_fMRI((reportRow.getTMRI_PctCompl()>=100) ? 1 : 0);
		reportRow.setFull_MR_Compl( (
										reportRow.getT1_Count()>=1 && 
										reportRow.getT2_Count()>=1 && 
										reportRow.getRS_fMRI_PctCompl()>=100 && 
										reportRow.getDMRI_Compl()>=1 && 
										reportRow.getFull_Task_fMRI()>=1 
									) ? 1 : 0);
	}

	private static Double getPctPairComplete(XnatImagescandataI scan) {
		if (scan == null) {
			return 0d;
		}
		return (scan.getPctpaircomplete() != null) ? scan.getPctpaircomplete() : 0d;
	}

	private static Double getPctComplete(XnatImagescandataI scan) {
		if (scan == null) {
			return 0d;
		}
		return (scan.getPctcomplete() != null) ? scan.getPctcomplete() : 0d;
	}
	
	private static double getPctComplete(XnatImagescandataI scan, int expectedFrames) {
		Double pctComplete = getPctComplete(scan);
		if (pctComplete == null || pctComplete == 0 || pctComplete.toString().isEmpty()) {
			pctComplete = (scan.getFrames().doubleValue()/expectedFrames)*100;
		}
		return pctComplete;
	}

	private static String getSubjectKey(XnatSubjectdata subject) {
		final Object subjectKeyObj = subject.getFieldByName("nda_guid");
		return (subjectKeyObj != null) ? subjectKeyObj.toString() : "";
	}

	private static String getSubjectGender(XnatSubjectdata subject) {
		final Object subjectGenderObj = subject.getFieldByName("nda_gender");
		return (subjectGenderObj != null) ? subjectGenderObj.toString() : "";
	}


	private static String getInterviewDate(XnatMrsessiondata mrSession) {
		final Object interviewDateObj = mrSession.getFieldByName("nda_interview_date");
		return (interviewDateObj != null) ? interviewDateObj.toString() : "";
	}

	private static String getInterviewAge(XnatMrsessiondata mrSession) {
		final Object interviewAgeObj = mrSession.getFieldByName("nda_interview_age");
		return (interviewAgeObj != null) ? interviewAgeObj.toString() : "";
	}

	private static String getVisitFromSessionLabel(String sessionLabel) {
		if (sessionLabel.contains("_V1_")) {
			return "V1";
		} else if (sessionLabel.contains("_V2_")) {
			return "V2";
		} else if (sessionLabel.contains("_V3_")) {
			return "V3";
		}
		return "UNKNOWN";
	}

}
