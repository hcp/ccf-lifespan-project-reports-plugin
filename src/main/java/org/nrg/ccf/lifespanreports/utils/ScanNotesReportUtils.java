package org.nrg.ccf.lifespanreports.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.ccf.lifespanreports.queries.LifespanReportsQueries;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.JdbcTemplate;

public class ScanNotesReportUtils {
	
	//final static SimpleDateFormat _df = new SimpleDateFormat("yyyy-MM-dd");
	final static String TOTAL = "_TOTAL_";
	final static String SPLITTER = ":::";
	final static DecimalFormat _numFormatter = new DecimalFormat("###.###");
	public static Logger _logger = Logger.getLogger(ScanNotesReportUtils.class);
	private static List<String> MAIN_SCANS = Arrays.asList(
			new String[] {"T1w","T1w_Norm","T2w","T2w_Norm","rfMRI","tfMRI","dMRI",
							"TSI_HiResHp","TSE_Norm_HiResHp","mbPCASLhr","mbPCASL"});
	private static List<String> REQUIRE_NOTES = Arrays.asList(
			new String[] {"rfMRI","tfMRI","dMRI"});

	public static List<Map<String,String>> getDataTable(final String projectId, final UserI user, JdbcTemplate _jdbcTemplate) throws InvalidPermissionException {
		
		final List<Map<String,String>> resultsList = new ArrayList<>();
		final String whereClause = "WHERE se.project = '" + projectId + "' and isc.type in " + getMainScanWhere();
		final String queryString = LifespanReportsQueries.MR_SCAN_QUERY.replace(LifespanReportsQueries.WHERE_REPLACE, whereClause);
		final List<Map<String,Object>> results =_jdbcTemplate.queryForList(queryString);
		for (Map<String,Object> qResult : results) {
			final Object scanTypeObj = qResult.get("scan_type");
			final String scanType = (scanTypeObj!=null) ? scanTypeObj.toString() : "";
			final Object scanNotesObj = qResult.get("scan_note");
			final String scanNotes = (scanNotesObj!=null) ? scanNotesObj.toString() : "";
			if (REQUIRE_NOTES.contains(scanType)) {
				if (scanNotes.length()<1) {
					continue;
				}
			}
			final Map<String,String> result = new LinkedHashMap<>();
			if (qResult.get("subj_id") != null) {
				result.put("subjId", qResult.get("subj_id").toString());
			}
			if (qResult.get("subj_label") != null) {
				result.put("subjLabel", qResult.get("subj_label").toString());
			}
			if (qResult.get("subj_age") != null) {
				result.put("subjAge", qResult.get("subj_age").toString());
			}
			if (qResult.get("exp_id") != null) {
				result.put("expId", qResult.get("exp_id").toString());
			}
			if (qResult.get("exp_label") != null) {
				result.put("expLabel", qResult.get("exp_label").toString());
			}
			if (qResult.get("scan_id") != null) {
				result.put("scanId", qResult.get("scan_id").toString());
			}
			if (qResult.get("scan_scanner") != null) {
				result.put("expScanner", qResult.get("scan_scanner").toString());
				result.put("expSite", getSiteValue(projectId,qResult.get("scan_scanner").toString()));
			}
			if (qResult.get("scan_type") != null) {
				result.put("scanType", qResult.get("scan_type").toString());
			}
			if (qResult.get("series_description") != null) {
				result.put("scanDesc", qResult.get("series_description").toString());
			}
			if (qResult.get("scan_quality") != null) {
				result.put("scanQuality", qResult.get("scan_quality").toString());
			}
			if (qResult.get("scan_frames") != null) {
				result.put("scanFrames", qResult.get("scan_frames").toString());
			}
			if (qResult.get("scan_note") != null) {
				result.put("scanNote", qResult.get("scan_note").toString());
			}
			if (qResult.get("exp_note") != null) {
				result.put("sessionNote", qResult.get("exp_note").toString());
			}
			resultsList.add(result);
		}
		return resultsList;
	}

	private static String getMainScanWhere() {
		final StringBuffer sb = new StringBuffer("(");
		final Iterator<String> i = MAIN_SCANS.iterator();
		while (i.hasNext()) {
			sb.append("'" + i.next() + "'");
			if (i.hasNext()) {
				sb.append(",");
			}
		}
		sb.append(")");
		return sb.toString();
	}

	private static String getSiteValue(String projectId, String scanner) {
		String site = null;
		if (projectId.equals("CCF_HCA_ITK") || projectId.equals("CCF_HCD_ITK")) {
			if (scanner.contains("67056")) {
				site = "Harvard";
			} else if (scanner.contains("35177") || scanner.contains("166038")) {
				site = "WUSTL";
			} else if (scanner.contains("35343") || scanner.contains("35426")) { 
				site = "UCLA";
			} else if (scanner.contains("BAY4OC")) {
				site = "MGH";
			} else if (scanner.contains("TRIOC") || scanner.contains("166007")) {
				site = "UMN";
			} else {
				site = "UNKNOWN";
			}
		}
		return site;
	}

}
