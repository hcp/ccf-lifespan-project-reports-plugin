package org.nrg.ccf.lifespanreports.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.ccf.lifespanreports.components.QcStat;
import org.nrg.ccf.lifespanreports.components.QcStatistics;
import org.nrg.ccf.lifespanreports.components.QcTables;
import org.nrg.ccf.lifespanreports.components.ScanInfo;
import org.nrg.ccf.lifespanreports.components.SubjectInfo;
import org.nrg.ccf.lifespanreports.queries.LifespanReportsQueries;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.om.XnatInvestigatordata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LifespanReportUtils {
	
	//final static SimpleDateFormat _df = new SimpleDateFormat("yyyy-MM-dd");
	final static String TOTAL = "_TOTAL_";
	final static DecimalFormat _numFormatter = new DecimalFormat("###.###");


	public static List<Map<String,String>> getQcSummaryTable(final String projectId, final UserI user, List<Map<String, String>> qcDataTable) throws InvalidPermissionException {
		if (projectId.equalsIgnoreCase("CCF_HCD_ITK")) {
			return getSummaryTable(projectId, user, qcDataTable, new String[] { "age", "T1R", "T1U", "T2R", "T2U", "REST_AP_1","REST_AP_1_ABS_RMS","REST_AP_1_REL_RMS", "REST_PA_1","REST_PA_1_ABS_RMS","REST_PA_1_REL_RMS", "GUESS_PA",
					"GUESS_AP", "CARIT_PA", "CARIT_AP", "EMOT", "MBPCASL", "REST_AP_2","REST_AP_2_ABS_RMS","REST_AP_2_REL_RMS", "REST_PA_2","REST_PA_2_ABS_RMS","REST_PA_2_REL_RMS", "DMRI", "COMPLETE" }, "site");
		}else if (projectId.equalsIgnoreCase("CCF_HCA_ITK")) {
			return getSummaryTable(projectId, user, qcDataTable, new String[] { "age", "T1R", "T1U", "T2R", "T2U", "REST_AP_1","REST_AP_1_ABS_RMS","REST_AP_1_REL_RMS", "REST_PA_1","REST_PA_1_ABS_RMS","REST_PA_1_REL_RMS", "VISMOTOR",
					"CARIT", "FACENAME", "MBPCASL", "REST_AP_2","REST_AP_2_ABS_RMS","REST_AP_2_REL_RMS", "REST_PA_2","REST_PA_2_ABS_RMS","REST_PA_2_REL_RMS", "DMRI", "COMPLETE" }, "site");
		}
		throw new InvalidPermissionException("ERROR:  This report is only available for LifeSpan HCA/HCD Intake projects.");
	}
	
	
	public static List<Map<String,String>> getQcDataTable(final String projectId, final UserI user) throws InvalidPermissionException {
		
		final JdbcTemplate jdbcTemplate = XDAT.getContextService().getBeanSafely(JdbcTemplate.class);
		
		final Map<XnatMrsessiondata,List<QcStat>> exps = getProjectMrSessions(projectId, user, jdbcTemplate);
		
		List<SubjectInfo> subjects = getSubjects(projectId, user, jdbcTemplate);
		
		List<ScanInfo> allScans = getScanInfo(projectId, user, jdbcTemplate);
				
		final List<Map<String, String>> rtn = processExperiments(projectId, user, exps, subjects, allScans);
	
		return rtn;
		
	}


	private static List<SubjectInfo> getSubjects(String projectId, UserI user, JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE s.project='" + projectId + "'";
		final String queryStr = LifespanReportsQueries.SUBJECT_INFO_QUERY.replace(LifespanReportsQueries.WHERE_REPLACE, whereStr);
		final List<SubjectInfo> subjects = jdbcTemplate.query(queryStr,new BeanPropertyRowMapper<SubjectInfo>(SubjectInfo.class));
		return subjects;
		
	}

	protected static List<ScanInfo> getScanInfo(String projectId, UserI user, JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE se.project='" + projectId + "'";
		final String queryStr = LifespanReportsQueries.ALL_SCAN_QUERY.replace(LifespanReportsQueries.WHERE_REPLACE, whereStr);
		final List<ScanInfo> scans = jdbcTemplate.query(queryStr,new BeanPropertyRowMapper<ScanInfo>(ScanInfo.class));
		return scans;
		
	}


	private static Map<XnatMrsessiondata, List<QcStat>> getProjectMrSessions(String projectId,
			UserI user, JdbcTemplate jdbcTemplate) {
		
		log.info("getProjectMrSessions INIT: " + new Date(System.currentTimeMillis()));
		final Map<XnatMrsessiondata,List<QcStat>> expMap = new HashMap<>();
		final CriteriaCollection expCC = new CriteriaCollection("OR");
		expCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(expCC, user, false);
		log.info("getProjectMrSessions mrData: " + new Date(System.currentTimeMillis()));
		
		final String whereStr = "WHERE exp.project='" + projectId + 
				"' and stat.name in ('MOTION_QC_ABS_MEAN_RMS','MOTION_QC_REL_MEAN_RMS')";
		final String queryStr = LifespanReportsQueries.QC_STATS_QUERY.replace(LifespanReportsQueries.WHERE_REPLACE, whereStr);
		final List<QcStat> qcStats = jdbcTemplate.query(queryStr,new BeanPropertyRowMapper<QcStat>(QcStat.class));
		for (final XnatMrsessiondata exp : exps) {
			final List<QcStat> qclist = new ArrayList<>();
			final Iterator<QcStat> i = qcStats.iterator();
			while (i.hasNext()) {
				final QcStat qc = i.next();
				if (qc.getExpId().equals(exp.getId())) {
					qclist.add(qc);
					i.remove();
				}
			}
			expMap.put(exp,qclist);
		}
		log.info("getProjectMrSessions processingComplete: " + new Date(System.currentTimeMillis()));
		return expMap;
		
	}

	private static SubjectInfo getSubject(String projectId, String subjectId, List<SubjectInfo> subjects) {
		for (final SubjectInfo subj : subjects) {
			if (subj.getId().equals(subjectId)) {
				return subj;
			}
		}
		return null;
	}

	private static List<Map<String, String>> processExperiments(String projectId, UserI user, Map<XnatMrsessiondata, List<QcStat>> exps,
			List<SubjectInfo> subjects, List<ScanInfo> allScans) throws InvalidPermissionException {
			
			final Map<String,Object> subjectDates = new HashMap<>();
			final Map<String,Map<String,String>> subjectMaps = new HashMap<>();
			final List<Map<String,String>> records = new ArrayList<>();
			
			log.debug("getQcDataTable - " + projectId);
			for (final XnatMrsessiondata exp : exps.keySet()) {
				final List<QcStat> qcList = exps.get(exp);
				//_logger.debug("Begin Processing - " + exp.getLabel());
				final String subjectId = exp.getSubjectId();
				final Map<String,String> subjectMap = getSubjectMap(projectId, subjectMaps, subjectId);
				final Object expDate = exp.getDate();
				final Object subjectDate = getSubjectDate(subjectDates, subjectId);
				if (subjectDate == null) {
					subjectDates.put(subjectId, subjectDate);
					//_logger.debug("processExperimentValues - " + exp.getLabel());
					processExperimentValues(exp, projectId, subjectId, subjectMap, subjects, user);
				} else if (subjectDate instanceof Date && expDate instanceof Date) {
					if (((Date)expDate).before((Date)subjectDate)) {
						//_logger.debug("processExperimentValues - " + exp.getLabel());
						processExperimentValues(exp, projectId, subjectId, subjectMap, subjects, user);
					}
				} else if (expDate != null) {
					if (expDate.toString().compareTo(subjectDate.toString()) < 0) {
						//_logger.debug("processExperimentValues - " + exp.getLabel());
						processExperimentValues(exp, projectId, subjectId, subjectMap, subjects, user);
					}
				}
				List<ScanInfo> expScanList = getScanInfo(exp, allScans);
				processSession(exp, qcList, expScanList, subjectMap, projectId);
			}
			records.addAll(subjectMaps.values());
			Collections.sort(records, new Comparator<Map<String, String>>() {
	
				@Override
				public int compare(Map<String, String> o1, Map<String, String> o2) {
					if (o1 != null && o1.get("subject") != null && o2 != null && o2.get("subject") != null) {
						return o1.get("subject").compareTo(o2.get("subject"));
					} else if (o1 == null || o1.get("subject") == null) {
						return -1;
					} else if (o2 == null || o2.get("subject") == null) {
						return 1;
					} else {
						return 0;
					}
				}
				
			});
			return records;
		
	}

	private static void processSession(XnatMrsessiondata exp, List<QcStat> qcList, List<ScanInfo> expScanList, Map<String, String> subjectMap, String projectId) {
		
		boolean hasTask = false;
		//boolean hasPcasl = false;
		boolean hasUsablePcasl = false;
		boolean hasUsableTask = false;
		boolean hasDiffusion = false;
		boolean hasUsableDiffusion = false;
		
		//_logger.debug("processSession - " + exp.getLabel());
		for (final ScanInfo scan : expScanList) {
			final String scanType = scan.getType();
			final String scanQuality = scan.getQuality();
			final boolean dontRelease = false; //scan.getReleaseoverride()!=2;
			if (scanType.equals("tfMRI")) {
				hasTask = true;
				if (!hasUsableTask && !scanQuality.equalsIgnoreCase("UNUSABLE")) {
					hasUsableTask = true;
				}
			} else if (scanType.equals("dMRI")) {
				hasDiffusion = true;
				if (!hasUsableDiffusion && !scanQuality.equalsIgnoreCase("UNUSABLE")) {
					hasUsableDiffusion = true;
				}
			} else if (scanType.equals("mbPCASL") || scanType.equals("mbPCASLhr")) {
				//hasPcasl = true;
				if (!hasUsablePcasl && !scanQuality.equalsIgnoreCase("UNUSABLE")) {
					hasUsablePcasl = true;
				}
			} else if (scanType.equals("T1w_Norm")) {
				//_logger.debug("processSession - SCANTYPE=" + scanType + ", QUALITY=" + scanQuality);
				if (!(scanQuality.equalsIgnoreCase("UNUSABLE") || scanQuality.equalsIgnoreCase("POOR") || dontRelease)) {
					subjectMap.put("T1U", "1");
					subjectMap.put("T1R", getQualityRating(scan, exp, expScanList));
				} else if (subjectMap.get("T1R").length()<1) {
					subjectMap.put("T1R", getQualityRating(scan, exp, expScanList));
				}
			} else if (scanType.equals("T2w_Norm")) {
				//_logger.debug("processSession - SCANTYPE=" + scanType + ", QUALITY=" + scanQuality);
				if (!(scanQuality.equalsIgnoreCase("UNUSABLE") || scanQuality.equalsIgnoreCase("POOR") || dontRelease)) {
					subjectMap.put("T2U", "1");
					subjectMap.put("T2R", getQualityRating(scan, exp, expScanList));
				} else if (subjectMap.get("T2R").length()<1) {
					subjectMap.put("T2R", getQualityRating(scan, exp, expScanList));
				}
			}
		}
		if (hasUsableTask) {
			for (final ScanInfo scan : expScanList) {
				final String scanType = scan.getType();
				final String seriesDesc = scan.getSeriesDescription();
				final String scanQuality = scan.getQuality();
				final Integer frames = scan.getFrames();
				if (!scanType.equalsIgnoreCase("tfMRI") || scanQuality.equalsIgnoreCase("UNUSABLE")) {
					continue;
				}
				if (projectId.equals("CCF_HCD_ITK")) {
					if (seriesDesc.equalsIgnoreCase("tfMRI_GUESSING_AP")) {
						if (frames>=280) {
							subjectMap.put("GUESS_AP", "1");
						}
					} else if (seriesDesc.equalsIgnoreCase("tfMRI_GUESSING_PA")) {
						if (frames>=280) {
							subjectMap.put("GUESS_PA", "1");
						}
					} else if (seriesDesc.equalsIgnoreCase("tfMRI_CARIT_AP")) {
						if (frames>=300) {
							subjectMap.put("CARIT_AP", "1");
						}
					} else if (seriesDesc.equalsIgnoreCase("tfMRI_CARIT_PA")) {
						if (frames>=300) {
							subjectMap.put("CARIT_PA", "1");
						}
					} else if (seriesDesc.equalsIgnoreCase("tfMRI_EMOTION_AP") || seriesDesc.equalsIgnoreCase("tfMRI_EMOTION_PA")) {
						if (frames>=178) {
							subjectMap.put("EMOT", "1");
						}
					}
				} else if (projectId.equals("CCF_HCA_ITK")) {
					if (seriesDesc.matches("^tfMRI_VISMOTOR_[AP][AP]$")) {
						if (frames>=194) {
							subjectMap.put("VISMOTOR", "1");
						}
					} else if (seriesDesc.matches("^tfMRI_CARIT_[AP][AP]$")) {
						if (frames>=300) {
							subjectMap.put("CARIT", "1");
						}
					} else if (seriesDesc.matches("^tfMRI_FACENAME_[AP][AP]$")) {
						if (frames>=345) {
							subjectMap.put("FACENAME", "1");
						}
					}
				}
			}
		}
		if (hasUsablePcasl) {
			for (final ScanInfo scan : expScanList) {
				final String scanType = scan.getType();
				final String seriesDesc = scan.getSeriesDescription();
				final String scanQuality = scan.getQuality();
				final Integer frames = scan.getFrames();
				if (!(scanType.equalsIgnoreCase("MBPCASL") || scanType.equalsIgnoreCase("MBPCASLHR")) ||
						scanQuality.equalsIgnoreCase("UNUSABLE")) {
					continue;
				}
				if (seriesDesc.matches("^mbPCASL.*_[AP][AP]$")) {
					if (frames>=90) {
						subjectMap.put("MBPCASL", "1");
					} else if (frames>=70 && scan.getType().equalsIgnoreCase("MBPCASL")) {
						subjectMap.put("MBPCASL", "0.5");
					}
				}
			}
		}
		for (final ScanInfo scan : expScanList) {
			QcStatistics qcStats = getQcStatsForScan(exp,scan,qcList);
			final String scanType = scan.getType();
			final String seriesDesc = scan.getSeriesDescription();
			final String scanQuality = scan.getQuality();
			final Integer frames = scan.getFrames();
			Double absMeanRms = null;
			Double relMeanRms = null;
			if (qcStats != null) {
				absMeanRms = qcStats.getAbsMeanRms();
				relMeanRms = qcStats.getRelMeanRms();
			}
			
			if (!scanType.equalsIgnoreCase("rfMRI") || scanQuality.equalsIgnoreCase("UNUSABLE")) {
				continue;
			}
			if (hasTask && !hasDiffusion) {
				if (seriesDesc.equalsIgnoreCase("rfMRI_REST_AP") && frames>=488) {
					if (!subjectMap.get("REST_AP_1").equals("1")) {
						subjectMap.put("REST_AP_1", "1");
						subjectMap.put("REST_AP_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_AP_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_AP_2").equals("1")) {
						subjectMap.put("REST_AP_2", "1");
						subjectMap.put("REST_AP_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_AP_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}else if (seriesDesc.equalsIgnoreCase("rfMRI_REST_PA") && frames>=488) {
					if (!subjectMap.get("REST_PA_1").equals("1")) {
						subjectMap.put("REST_PA_1", "1");
						subjectMap.put("REST_PA_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_PA_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_PA_2").equals("1")) {
						subjectMap.put("REST_PA_2", "1");
						subjectMap.put("REST_PA_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_PA_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}
			} else if (!hasTask && hasDiffusion) {
				if (seriesDesc.equalsIgnoreCase("rfMRI_REST_AP") && frames>=488) {
					if (!subjectMap.get("REST_AP_2").equals("1")) {
						subjectMap.put("REST_AP_2", "1");
						subjectMap.put("REST_AP_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_AP_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_AP_1").equals("1")) {
						subjectMap.put("REST_AP_1", "1");
						subjectMap.put("REST_AP_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_AP_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}else if (seriesDesc.equalsIgnoreCase("rfMRI_REST_PA") && frames>=488) {
					if (!subjectMap.get("REST_PA_2").equals("1")) {
						subjectMap.put("REST_PA_2", "1");
						subjectMap.put("REST_PA_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_PA_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_PA_1").equals("1")) {
						subjectMap.put("REST_PA_1", "1");
						subjectMap.put("REST_PA_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
						subjectMap.put("REST_PA_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}
			} else {
				if (seriesDesc.equalsIgnoreCase("rfMRI_REST_AP") && frames>=488) {
					if (!subjectMap.get("REST_AP_1").equals("1")) {
							subjectMap.put("REST_AP_1", "1");
							subjectMap.put("REST_AP_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
							subjectMap.put("REST_AP_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_AP_2").equals("1")) {
							subjectMap.put("REST_AP_2", "1");
							subjectMap.put("REST_AP_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
							subjectMap.put("REST_AP_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}else if (seriesDesc.equalsIgnoreCase("rfMRI_REST_PA") && frames>=488) {
					if (!subjectMap.get("REST_PA_1").equals("1")) {
							subjectMap.put("REST_PA_1", "1");
							subjectMap.put("REST_PA_1_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
							subjectMap.put("REST_PA_1_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					} else if (!subjectMap.get("REST_PA_2").equals("1")) {
							subjectMap.put("REST_PA_2", "1");
							subjectMap.put("REST_PA_2_ABS_RMS", (absMeanRms!=null) ? absMeanRms.toString() : "");
							subjectMap.put("REST_PA_2_REL_RMS", (relMeanRms!=null) ? relMeanRms.toString() : "");
					}
				}
			}
		}
		if (hasUsableDiffusion) {
			boolean dir98_ap = false;
			boolean dir98_pa = false;
			boolean dir99_ap = false;
			boolean dir99_pa = false;
			for (final ScanInfo scan : expScanList) {
				final String scanType = scan.getType();
				final String seriesDesc = scan.getSeriesDescription();
				final String scanQuality = scan.getQuality();
				final Integer frames = scan.getFrames();
				if (!scanType.equalsIgnoreCase("dMRI") || scanQuality.equalsIgnoreCase("UNUSABLE")) {
					continue;
				}
				if (seriesDesc.equalsIgnoreCase("dMRI_dir98_AP") && frames>=99) {
					dir98_ap = true;
				} else if (seriesDesc.equalsIgnoreCase("dMRI_dir98_PA") && frames>=99) {
					dir98_pa = true;
				} else if (seriesDesc.equalsIgnoreCase("dMRI_dir99_AP") && frames>=100) {
					dir99_ap = true;
				} else if (seriesDesc.equalsIgnoreCase("dMRI_dir99_PA") && frames>=100) {
					dir99_pa = true;
				}
			}
			if (dir98_ap && dir98_pa && dir99_ap && dir99_pa) {
				subjectMap.put("DMRI", "1");
			}
		}
		if (projectId.equals("CCF_HCD_ITK")) {
			if (subjectMap.get("T1U").equals("1") &&
			    subjectMap.get("T2U").equals("1") &&
			    subjectMap.get("REST_AP_1").equals("1") &&
			    subjectMap.get("REST_PA_1").equals("1") &&
			    subjectMap.get("GUESS_AP").equals("1") &&
			    subjectMap.get("GUESS_PA").equals("1") &&
			    subjectMap.get("CARIT_AP").equals("1") &&
			    subjectMap.get("CARIT_PA").equals("1") &&
			    subjectMap.get("EMOT").equals("1") &&
			    (subjectMap.get("MBPCASL").equals("1") || subjectMap.get("MBPCASL").equals("0.5")) &&
			    subjectMap.get("REST_AP_2").equals("1") &&
			    subjectMap.get("REST_PA_2").equals("1") &&
			    subjectMap.get("DMRI").equals("1")
			    ) {
				subjectMap.put("COMPLETE", "1");
			}
		}else if (projectId.equals("CCF_HCA_ITK")) {
			if (subjectMap.get("T1U").equals("1") &&
			    subjectMap.get("T2U").equals("1") &&
			    subjectMap.get("REST_AP_1").equals("1") &&
			    subjectMap.get("REST_PA_1").equals("1") &&
			    subjectMap.get("VISMOTOR").equals("1") &&
			    subjectMap.get("CARIT").equals("1") &&
			    subjectMap.get("FACENAME").equals("1") &&
			    (subjectMap.get("MBPCASL").equals("1") || subjectMap.get("MBPCASL").equals("0.5")) &&
			    subjectMap.get("REST_AP_2").equals("1") &&
			    subjectMap.get("REST_PA_2").equals("1") &&
			    subjectMap.get("DMRI").equals("1")
			    ) {
				subjectMap.put("COMPLETE", "1");
			}
		}
		
	}

	private static List<ScanInfo> getScanInfo(XnatMrsessiondata exp, List<ScanInfo> allScans) {
		final List<ScanInfo> scanList = new ArrayList<>();
		final Iterator<ScanInfo> i = allScans.iterator();
		while (i.hasNext()) {
			final ScanInfo scanInfo = i.next();
			if (scanInfo.getImageSessionId().equals(exp.getId())) {
				scanList.add(scanInfo);
				i.remove();
			}
		}
		return scanList;
	}


	private static QcStatistics getQcStatsForScan(XnatMrsessiondata exp, ScanInfo scan, List<QcStat> qcList) {
		final QcStatistics qcStatistics = new QcStatistics(exp,scan);
		for (final QcStat qcStat : qcList) {
			if (qcStatistics.getExpId().equals(qcStat.getExpId()) && qcStatistics.getScanId().equals(qcStat.getScanId())) {
				if (qcStat.getStatName().equals("MOTION_QC_ABS_MEAN_RMS")) {
					qcStatistics.setAbsMeanRms(qcStat.getStatValue());
				} else if (qcStat.getStatName().equals("MOTION_QC_REL_MEAN_RMS")) {
					qcStatistics.setRelMeanRms(qcStat.getStatValue());
				}
			}
		}
		return qcStatistics;
	}


	private static String getQualityRating(ScanInfo scan, XnatMrsessiondata exp, List<ScanInfo> scanList) {
		if (scan == null) {
			return "";
		}
		final String note = scan.getNote();
		String rating = (note != null) ? note.replaceAll("^.*[Ee][Kk][Rr]:","") : "";
		rating = rating.replaceAll("[^0-9. ]", "").trim();
		int space = rating.indexOf(' ');
		if (space>0) {
			rating = rating.substring(0, space);
		}
		Boolean isNorm = scan.getType().contains("_Norm");
		try { 
			Double ratingF = Double.parseDouble(rating);
			if (isNorm && (ratingF == null || ratingF < 0 || ratingF >4)) {
				rating = getQualityRating(getPreviousScan(scan, exp, scanList), exp, scanList);
			}
		} catch (Exception e) {
			if (isNorm) {
				rating = getQualityRating(getPreviousScan(scan, exp, scanList), exp, scanList);
			}
		}
		return rating;
	}

	private static ScanInfo getPreviousScan(ScanInfo scan, XnatMrsessiondata exp, List<ScanInfo> scanList) {
		final Object[] scanArr = scanList.toArray();
		for (int i=0; i<scanArr.length; i++) {
			if (!(scanArr[i] instanceof XnatImagescandataI)) {
				continue;
			}
			final XnatImagescandataI tScan = (XnatImagescandataI)scanArr[i];
			if (scan.getId().equals(tScan.getId()) && scan.getSeriesDescription().equals(tScan.getSeriesDescription())) {
				if (i<1 || !(scanArr[i-1] instanceof XnatImagescandataI)) {
					continue;
				}
				final ScanInfo pScan = (ScanInfo)scanArr[i-1];
				final String pScanType = pScan.getType();
				if (pScanType.equals("T1w") || pScanType.equals("T2w")) {
					return pScan;
				}
			}
		}
		return null;
	}

	private static void processExperimentValues(XnatMrsessiondata exp, String projectId, String subjectId, Map<String, String> subjectMap, List<SubjectInfo> subjects, UserI user) throws InvalidPermissionException {
		final Object expDate = exp.getDate();
		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if (expDate instanceof Date) {
			subjectMap.put("date", df.format(expDate));
		} else if (expDate != null) {
			subjectMap.put("date", expDate.toString());
		} else {
			subjectMap.put("date", "");
		}
		final String scanner = exp.getScanner();
		subjectMap.put("scanner", scanner);
		String site = "";
		if (scanner != null) {
			if (scanner.contains("67056")) {
				site = "Harvard";
			} else if (scanner.contains("35177") || scanner.contains("166038")) {
				site = "WUSTL";
			} else if (scanner.contains("35343") || scanner.contains("35426")) { 
				site = "UCLA";
			} else if (scanner.contains("BAY4OC")) {
				site = "MGH";
			} else if (scanner.contains("TRIOC") || scanner.contains("166007")) {
				site = "UMN";
			}
		}
		subjectMap.put("site", site);
		SubjectInfo subjectData = getSubject(projectId, subjectId, subjects);
		if (subjectData == null) {
			return;
		}
		Integer age = null;
		try {
			if (subjectData.getAge() != null) {
				age = Integer.valueOf(subjectData.getAge());
			}
		} catch (Exception e) {
			// Do nothing
		}
		if (age != null) {
			subjectMap.put("age", subjectData.getAge().toString());
		} else {
			subjectMap.put("age", "");
		}
		if (subjectData.getLabel() != null) {
			subjectMap.put("subject", subjectData.getLabel());
		}
	}

	/*
	private static void processHcdScan(final XnatImagescandataI scan, final Map<String, String> subjectMap, 
			XnatMrsessiondata exp, boolean hasTask, boolean hasDiffusion) {
		if (subjectMap.containsKey("GUESS_PA")) {
			if ()
		}
		
	}
	*/

	private static Map<String,String> getSubjectMap(String projectId, final Map<String,Map<String, String>> subjectMaps, 
			final String subjectId) throws InvalidPermissionException {
		if (!subjectMaps.containsKey(subjectId)) {
			final Map<String, String> subjectMap = new HashMap<>();
			if (projectId.equals("CCF_HCD_ITK")) {
				subjectMap.put("T1R","");
				subjectMap.put("T1U","0");
				subjectMap.put("T2R","");
				subjectMap.put("T2U","0");
				subjectMap.put("REST_AP_1","0");
				subjectMap.put("REST_AP_1_ABS_RMS","");
				subjectMap.put("REST_AP_1_REL_RMS","");
				subjectMap.put("REST_PA_1","0");
				subjectMap.put("REST_PA_1_ABS_RMS","");
				subjectMap.put("REST_PA_1_REL_RMS","");
				subjectMap.put("GUESS_PA","0");
				subjectMap.put("GUESS_AP","0");
				subjectMap.put("CARIT_PA","0");
				subjectMap.put("CARIT_AP","0");
				subjectMap.put("EMOT","0");
				subjectMap.put("MBPCASL","0");
				subjectMap.put("REST_AP_2","0");
				subjectMap.put("REST_AP_2_ABS_RMS","");
				subjectMap.put("REST_AP_2_REL_RMS","");
				subjectMap.put("REST_PA_2","0");
				subjectMap.put("REST_PA_2_ABS_RMS","");
				subjectMap.put("REST_PA_2_REL_RMS","");
				subjectMap.put("DMRI","0");
				subjectMap.put("COMPLETE","0");
			}else if (projectId.equals("CCF_HCA_ITK")) {
				subjectMap.put("T1R","");
				subjectMap.put("T1U","0");
				subjectMap.put("T2R","");
				subjectMap.put("T2U","0");
				subjectMap.put("REST_AP_1","0");
				subjectMap.put("REST_AP_1_ABS_RMS","");
				subjectMap.put("REST_AP_1_REL_RMS","");
				subjectMap.put("REST_PA_1","0");
				subjectMap.put("REST_PA_1_ABS_RMS","");
				subjectMap.put("REST_PA_1_REL_RMS","");
				subjectMap.put("VISMOTOR","0");
				subjectMap.put("CARIT","0");
				subjectMap.put("FACENAME","0");
				subjectMap.put("MBPCASL","0");
				subjectMap.put("REST_AP_2","0");
				subjectMap.put("REST_AP_2_ABS_RMS","");
				subjectMap.put("REST_AP_2_REL_RMS","");
				subjectMap.put("REST_PA_2","0");
				subjectMap.put("REST_PA_2_ABS_RMS","");
				subjectMap.put("REST_PA_2_REL_RMS","");
				subjectMap.put("DMRI","0");
				subjectMap.put("COMPLETE","0");
			} else {
				throw new InvalidPermissionException("ERROR:  This report is only available for LifeSpan HCA/HCD Intake projects.");
			}
			subjectMaps.put(subjectId, subjectMap);
		}
		return subjectMaps.get(subjectId);
	}

	private static Object getSubjectDate(Map<String, Object> subjectDates, String subjectId) {
		if (!subjectDates.containsKey(subjectId)) {
			subjectDates.put(subjectId, null);
		}
		return subjectDates.get(subjectId);
	}

	private static List<Map<String,String>> getSummaryTable(final String projectId, final UserI user, 
			List<Map<String, String>> qcDataTable, final String[] vars, final String groupVar) throws InvalidPermissionException {
		final List<Map<String,String>> records = new ArrayList<>();
		if (qcDataTable == null) {
			qcDataTable = getQcDataTable(projectId, user);
		}
		final Map<String,Map<String,List<Double>>> processMap = new HashMap<>();
		for (final Map<String, String> entry : qcDataTable) {
			final String group = entry.get(groupVar);
			if (group == null || group.length()<1) {
				continue;
			}
			final Map<String, List<Double>> groupMap = getGroupMap(processMap, group);
			final Map<String, List<Double>> totalMap = getGroupMap(processMap, TOTAL);
			for (final String var : vars) {
				final List<Double> varList = getVarList(groupMap, var);
				final List<Double> totalVarList = getVarList(totalMap, var);
				Double val;
				try {
					val = Double.valueOf(entry.get(var));
				} catch (NumberFormatException | NullPointerException e) {
					val = Double.NEGATIVE_INFINITY;
				}
				varList.add(val);
				totalVarList.add(val);
			}
		} 
		for (final String pGroup : processMap.keySet()) {
			final Map<String, String> summaryRecord = new HashMap<>();
			summaryRecord.put(groupVar, pGroup);
			final Map<String, List<Double>> groupMap = getGroupMap(processMap, pGroup); 
			for (final String var : vars) {
				final List<Double> grpList = groupMap.get(var);
				final List<Double> varList = new ArrayList<>();
				for (Double f : grpList) {
					// For Statistics purposes, we'll treat 0.5's as 1's.
					if (!var.toUpperCase().contains("MBPCASL")) {
						varList.add(f);
					} else { 
						if (f!= null && (f>0 && f<1)) {
							varList.add(Double.valueOf("1"));
						} else {
							varList.add(f);
						}
					}
				}
				final Map<String, Double> statMap = getStatisticsMap(varList);
				for (final String stat : statMap.keySet()) {
					final Double value = statMap.get(stat);
					summaryRecord.put(var + "_" + stat,_numFormatter.format(value));
				}
			}
			records.add(summaryRecord);
		}
		Collections.sort(records, new Comparator<Map<String, String>>() {

			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				final String o1group = o1.get(groupVar);
				final String o2group = o2.get(groupVar);
				final String o1Compare = (o1group != null && !o1group.equals(TOTAL)) ? o1group : "ZZZZZZZ";
				final String o2Compare = (o2group != null && !o2group.equals(TOTAL)) ? o2group : "ZZZZZZZ";
				return o1Compare.compareTo(o2Compare);
			}
			
		});
		return records;
	}

	private static Map<String, Double> getStatisticsMap(List<Double> valueList) {
		final Map<String, Double> statsList = new HashMap<>(); 
		statsList.put("N", (double)(valueList.size()));
		statsList.put("SUM", getSum(valueList));
		statsList.put("MEAN", getMean(valueList));
		return statsList;
	}

	private static Double getMean(List<Double> valueList) {
		final Double sum = getSum(valueList);
		double n = 0;
		for (final Double f : valueList) {
			if (f != null && f > Double.NEGATIVE_INFINITY) {
				n = n + 1;
			}
		}
		return sum/n;
	}

	private static Double getSum(List<Double> valueList) {
		double sum = 0;
		for (final Double f : valueList) {
			if (f != null && f > Double.NEGATIVE_INFINITY) {
				sum = sum + f;
			}
		}
		return sum;
	}

	private static List<Double> getVarList(Map<String, List<Double>> groupMap, String var) {
		if (!groupMap.containsKey(var)) {
			groupMap.put(var,  new ArrayList<Double>());
		}
		return groupMap.get(var);
	}

	private static Map<String, List<Double>> getGroupMap(Map<String, Map<String, List<Double>>> processMap, String group) {
		if (!processMap.containsKey(TOTAL)) {
			processMap.put(TOTAL, new HashMap<String, List<Double>>());
		}
		if (!processMap.containsKey(group)) {
			processMap.put(group, new HashMap<String, List<Double>>());
		}
		return processMap.get(group);
	}

	public static List<Map<String, String>> getSubjectSessionCounts(UserI user) {
		final List<Map<String, String>> returnList = new ArrayList<>();
		final ArrayList<XnatProjectdata> projects = XnatProjectdata.getAllXnatProjectdatas(user, false);
		Collections.sort(projects,new Comparator<XnatProjectdata>() {
			@Override
			public int compare(XnatProjectdata o1, XnatProjectdata o2) {
				return o1.getId().compareTo(o2.getId());
			}
		});
        final CriteriaCollection subC = new CriteriaCollection("OR");
        final CriteriaCollection expC = new CriteriaCollection("OR");
        final List<String> pList = new ArrayList<>();
        final Map<String,Integer> subjCounts = new HashMap<>();
        final Map<String,Integer> expCounts = new HashMap<>();
		for (final XnatProjectdata project : projects) {
			try {
				final String projectId = project.getId();
				if (projectId.startsWith("CCF_") && Permissions.canReadProject(user, projectId)) {
					final Map<String, String> infoMap = new HashMap<>();
					pList.add(projectId);
					infoMap.put("ProjectID", projectId);
					infoMap.put("ProjectType", getProjectType(projectId));
					infoMap.put("ProjectName", project.getName());
					final XnatInvestigatordata pi = project.getPi();
					final String piS = ((pi.getFirstname() != null) ? pi.getFirstname() + " " : "") + pi.getLastname();
					infoMap.put("ProjectPI", piS);
			        subC.addClause(XnatSubjectdata.SCHEMA_ELEMENT_NAME + "/project", projectId);
			        subC.addClause(XnatSubjectdata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
			        expC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
			        //expC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
					returnList.add(infoMap);
				}
			} catch (Exception e) {
				log.error("Exception thrown reading project permissions (PROJECT=" + project.getId() + "):  ", e);
			}
		}
		for (final String projectID : pList) {
			subjCounts.put(projectID, 0);
			expCounts.put(projectID, 0);
		}
		final List<XnatSubjectdata> subs=XnatSubjectdata.getXnatSubjectdatasByField(subC, user, false);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(expC, user, false);
		for (final XnatSubjectdata subj : subs) {
			final String proj = subj.getProject();
			if (pList.contains(proj) ) {
				incrementCounts(subjCounts,proj);
			}
			final List<XnatProjectparticipantI> shares = subj.getSharing_share();
			for (XnatProjectparticipantI share : shares) {
				String shareProj = share.getProject();
					if (pList.contains(shareProj) ) {
						incrementCounts(subjCounts,shareProj);
					}
			}
		}
		for (final XnatMrsessiondata exp : exps) {
			final String proj = exp.getProject();
			if (pList.contains(proj) ) {
				incrementCounts(expCounts,proj);
			}
		}
		for (final Map<String,String> infoMap : returnList) {
			final String projectId = infoMap.get("ProjectID");
			infoMap.put("Subjects", String.valueOf(subjCounts.get(projectId)));
			infoMap.put("Sessions", String.valueOf(expCounts.get(projectId)));
		}
		return returnList;
	}

	private static String getProjectType(String projectId) {
		final String pid = projectId.toUpperCase();
		if (pid.endsWith("_ITK")) {
			return "INTAKE";
		} else if (pid.endsWith("_PRC")) {
			return "PROCESSING";
		} else if (pid.endsWith("_PLT")) {
			return "PILOT";
		} else if (pid.endsWith("_STG")) {
			return "STAGING";
		} else {
			return "OTHER";
		}
	}

	private static void incrementCounts(Map<String, Integer> counts, String proj) {
		final Integer currentCount = counts.get(proj);
		counts.put(proj, currentCount+1);
	}


	public static QcTables getQcTables(String projectId, UserI sessionUser) throws InvalidPermissionException {
		
		final List<Map<String,String>> qcDataTable = getQcDataTable(projectId, sessionUser);
		final List<Map<String,String>> qcSummaryTable = getQcSummaryTable(projectId, sessionUser, qcDataTable);
		
		QcTables qcTables = new QcTables();
		qcTables.setQcDataTable(qcDataTable);
		qcTables.setQcSummaryTable(qcSummaryTable);
		return qcTables;
		
	}

}
