package org.nrg.ccf.lifespanreports.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.nrg.ccf.lifespanreports.queries.LifespanReportsQueries;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.collect.Table.Cell;
import com.google.common.collect.TreeBasedTable;

public class ScanQualityReportUtils {
	
	//final static SimpleDateFormat _df = new SimpleDateFormat("yyyy-MM-dd");
	final static String TOTAL = "_TOTAL_";
	final static String SPLITTER = ":::";
	final static DecimalFormat _numFormatter = new DecimalFormat("###.###");
	public static Logger _logger = Logger.getLogger(ScanQualityReportUtils.class);


	public static Map<String, List<Map<String,String>>> getSummaryTables(final String projectId, final UserI user, JdbcTemplate _jdbcTemplate) throws InvalidPermissionException {
		final Map<String,List<Map<String, String>>> resultsMap = new LinkedHashMap<>();
		final List<Map<String,String>> dataTable = getDataTable(projectId, user, _jdbcTemplate);
		final TreeBasedTable<String, String, Integer> allSites = TreeBasedTable.create();
		final TreeBasedTable<String, String, Double> allSitesPct = TreeBasedTable.create();
		final TreeBasedTable<String, String, Integer> bySites = TreeBasedTable.create();
		final TreeBasedTable<String, String, Double> bySitesPct = TreeBasedTable.create();
		for (Map<String,String> result : dataTable) {
			final String scanQuality = result.get("scanQuality");
			final String expSite = result.get("expSite");
			final String scanType = result.get("scanType");
			final String siteQuality = expSite + SPLITTER + scanQuality;
			Object cellObj = allSites.get(scanQuality,scanType);
			if (cellObj == null) {
				allSites.put(scanQuality, scanType, Integer.valueOf(1));
			} else {
				Integer i = (Integer)cellObj;
				allSites.put(scanQuality, scanType, i+1);
			}
			if (expSite==null || expSite.length()<1) {
				break;
			}
			Object bsCellObj = bySites.get(siteQuality,scanType);
			if (bsCellObj == null) {
				bySites.put(siteQuality, scanType, Integer.valueOf(1));
			} else {
				Integer i = (Integer)bsCellObj;
				bySites.put(siteQuality, scanType, i+1);
			}
		}
		final Map<String, Integer> colSums = new LinkedHashMap<>();
		for (final String allKey : allSites.columnKeySet()) {
			final Map<String, Integer> column = allSites.column(allKey);
			int colSum = 0;
			for (Integer i : column.values()) colSum += i;
			colSums.put(allKey, colSum);
		}
		for (final String colKey : colSums.keySet()) {
			allSites.put(TOTAL, colKey, colSums.get(colKey));
		}
		for (final Cell<String, String, Integer> cell : allSites.cellSet()) {
			allSitesPct.put(cell.getRowKey(), cell.getColumnKey(), (double)cell.getValue()/(double)colSums.get(cell.getColumnKey()));
		}
		List<TreeBasedTable<String,String,?>> tbcList = new ArrayList<>();
		tbcList.add(allSites);
		tbcList.add(allSitesPct);
		resultsMap.put("Quality Ratings by Scan Type",convertToListOfMaps(tbcList,new String[] {"Quality"}));
		if (bySites.rowKeySet().size()>0) {
			final Map<String, Map<String, Integer>> colSums2 = new LinkedHashMap<>();
			for (final String allKey : bySites.columnKeySet()) {
				final Map<String, Integer> column = bySites.column(allKey);
				final Map<String, Integer> siteValueMap = new LinkedHashMap<>();
				for (String siteQuality : column.keySet()) {
					String site = siteQuality.split(SPLITTER)[0];
					if (!siteValueMap.containsKey(site)) {
						siteValueMap.put(site, column.get(siteQuality));
					} else {
						Integer currentValue = siteValueMap.get(site);
						currentValue = currentValue + column.get(siteQuality);
						siteValueMap.put(site, currentValue);
					}
				}
				colSums2.put(allKey, siteValueMap);
			}
			for (Cell<String, String, Integer> cell : bySites.cellSet()) {
				bySitesPct.put(cell.getRowKey(), cell.getColumnKey(), (double)cell.getValue()/
						(double)(colSums2.get(cell.getColumnKey())).get(cell.getRowKey().split(SPLITTER)[0]));
			}
			tbcList.clear();
			tbcList.add(bySites);
			tbcList.add(bySitesPct);
			resultsMap.put("Quality Ratings by Scan Type grouped by Site",convertToListOfMaps(tbcList,new String[] {"Site","Quality"}));
		}
		return resultsMap;
	}
	
	private static List<Map<String,String>> convertToListOfMaps(final List<TreeBasedTable<String, String, ?>> tableList, final String[] rowLabels) {
		List<Map<String,String>> returnList = new ArrayList<>();
		if (tableList.size()<1) {
			return null;
		}
		final TreeBasedTable<String, String, ?> initialTable = tableList.get(0);
		for (final String rowKey : initialTable.rowKeySet()) {
			final Map<String,String> rowMap = new LinkedHashMap<>();
			final String[] splitVal = rowKey.split(SPLITTER);
			int counter = 0;
			for (final String label : Arrays.asList(rowLabels)) {
				try { 
					rowMap.put(label, splitVal[counter]);
				} catch (Exception e) {
					rowMap.put(label, "");
				}
				counter+=1;
			}
			for (final String colKey : initialTable.columnKeySet()) {
				StringBuilder sb = new StringBuilder();
				boolean isInitialTable = true;
				for (TreeBasedTable<String,String,?> table : tableList) {
					Object cellValue = table.get(rowKey, colKey);
					if (isInitialTable) {
						sb.append(getFormattedValue(cellValue,true));
					} else {
						sb.append(" (").append(getFormattedValue(cellValue,true)).append(")");
					}
					isInitialTable = false;
				}
				String outValue = sb.toString();
				if (outValue.replaceAll("[ ()]", "").isEmpty()) {
					outValue="";
				}
				rowMap.put(colKey, outValue);
			}
			returnList.add(rowMap);
		}
		Collections.sort(returnList,new Comparator<Map<String,String>>() {
			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				for (int i=0; i<rowLabels.length; i++) {
					if (!(rowLabels[i].equalsIgnoreCase("Quality"))) {
						int rtn = o1.get(rowLabels[i]).compareTo(o2.get(rowLabels[i]));
						if (rtn!=0) {
							return rtn;
						}
					} else {
						int rtn = qualitySortValue(o1.get("Quality")).compareTo(qualitySortValue(o2.get("Quality")));
						if (rtn != 0) {
							return rtn;
						}
						rtn = o1.get("Quality").compareTo(o2.get("Quality"));
						if (rtn != 0) {
							return rtn;
						}
					}
				}
				return 0;
			}
		});
		return returnList;
	}
	
	private static Integer qualitySortValue(String string) {
		if (string==null) {
			return -99;
		}
		final String cmp = string.toLowerCase();
		switch(cmp) {
			case "excellent":
					return 1;
			case "good":
					return 2;
			case "fair":
					return 3;
			case "poor":
					return 4;
			case "undetermined":
					return 5;
			case "usable":
					return 6;
			case "unusable":
					return 7;
			case TOTAL:
					return 8;
			default:
					return 99; 
		}
	}

	private static Object getFormattedValue(Object cellValue, boolean zeroForNull) {
		if (cellValue == null) {
			return (zeroForNull) ? "0" : "";
		} else if (cellValue instanceof Integer) {
			return cellValue.toString();
		} else if (cellValue instanceof Double) {
			return _numFormatter.format(((Double)cellValue*100));
		}
		return cellValue.toString();
	}

	public static List<Map<String,String>> getDataTable(final String projectId, final UserI user, JdbcTemplate _jdbcTemplate) throws InvalidPermissionException {
		
		final List<Map<String,String>> returnList = new ArrayList<>();
		final String whereClause = "WHERE se.project = '" + projectId + "'";
		final String queryString = LifespanReportsQueries.MR_SCAN_QUERY.replace(LifespanReportsQueries.WHERE_REPLACE, whereClause);
		final List<Map<String,Object>> results =_jdbcTemplate.queryForList(queryString);
		final Map<String,Map<String,Integer>> _scanTypeCounts = new LinkedHashMap<>();
		final Map<String,Map<String,Double>> _scanTypeHighest = new LinkedHashMap<>();
		final Map<String,Integer> _scanCount = new LinkedHashMap<>();
		final List<Map<String,String>> expResults = new ArrayList<>();
		for (Map<String,Object> qResult : results) {
			final Map<String,String> result = new LinkedHashMap<>();
			if (qResult.get("subj_id") != null) {
				result.put("subjId", qResult.get("subj_id").toString());
			}
			if (qResult.get("subj_label") != null) {
				result.put("subjLabel", qResult.get("subj_label").toString());
			}
			if (qResult.get("subj_age") != null) {
				result.put("subjAge", qResult.get("subj_age").toString());
			}
			if (qResult.get("exp_id") != null) {
				result.put("expId", qResult.get("exp_id").toString());
			}
			if (qResult.get("exp_label") != null) {
				result.put("expLabel", qResult.get("exp_label").toString());
			}
			if (qResult.get("scan_id") != null) {
				result.put("scanId", qResult.get("scan_id").toString());
			}
			if (qResult.get("scan_scanner") != null) {
				result.put("expScanner", qResult.get("scan_scanner").toString());
				result.put("expSite", getSiteValue(projectId,qResult.get("scan_scanner").toString()));
			}
			if (qResult.get("scan_type") != null) {
				result.put("scanType", qResult.get("scan_type").toString());
			}
			if (qResult.get("series_description") != null) {
				result.put("scanDesc", qResult.get("series_description").toString());
			}
			if (qResult.get("scan_quality") != null) {
				result.put("scanQuality", qResult.get("scan_quality").toString());
			}
			if (qResult.get("scan_frames") != null) {
				result.put("scanFrames", qResult.get("scan_frames").toString());
			}
			if (qResult.get("scan_note") != null) {
				final String scan_note = qResult.get("scan_note").toString();
				result.put("scanNote", scan_note);
				Double rating = getQualityRating(scan_note);
				result.put("scanRating", (rating!=null) ? rating.toString() : "");
			}
			if (qResult.get("exp_note") != null) {
				result.put("sessionNote", qResult.get("exp_note").toString());
			}
			expResults.add(result);
		}
		final Map<String,Integer> typeCounts = new LinkedHashMap<>();
		Integer expCount = 0;
		for (int i=0; i<expResults.size(); i++) {
			final Map<String, String> expResult = expResults.get(i);
			Map<String, String> prevResult = null;
			if (i>0) {
				prevResult = expResults.get(i-1);
			}
			if (i==0 || prevResult==null || prevResult.get("expId")==null || !prevResult.get("expId").equals(expResult.get("expId"))) {
				expCount+=1;
			}
			final String note = expResult.get("scanNote");
			if (note != null && note.matches("^.*[Ee][Kk][Rr][:].*$")) {
				final String scanType = expResult.get("scanType");
				if (typeCounts.keySet().contains(scanType)) {
					final Integer k = typeCounts.get(scanType);
					typeCounts.put(scanType, k+1);
				} else {
					typeCounts.put(scanType, 1);
				}
			}
		}
		Iterator<Entry<String, Integer>> i = typeCounts.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String,Integer> e = i.next();
			if (e.getValue() < ((double)expCount/8)) {
				i.remove();
			}
		}
		for (Map<String,String> expResult : expResults) {
			final String scanType = expResult.get("scanType");
			if (typeCounts.keySet().contains(scanType)) {
				returnList.add(expResult);
			} 
		}
		for (int j=0; j<returnList.size(); j++) {
			final Map<String, String> result = returnList.get(j);
			Map<String, String> prevResult = null;
			if (j>0) {
				prevResult = returnList.get(j-1);
			}
			final String subjId = result.get("subjId");
			final Map<String,Integer> scanTypeCounts;
			final Map<String,Double> scanTypeHighest;
			Integer scanCount;
			if (_scanTypeCounts.containsKey(subjId)) {
				scanTypeCounts = _scanTypeCounts.get(subjId);
				
			} else {
				scanTypeCounts = new LinkedHashMap<>();
				_scanTypeCounts.put(subjId, scanTypeCounts);
			}
			if (_scanTypeHighest.containsKey(subjId)) {
				scanTypeHighest = _scanTypeHighest.get(subjId);
				
			} else {
				scanTypeHighest = new LinkedHashMap<>();
				_scanTypeHighest.put(subjId, scanTypeHighest);
			}
			if (_scanCount.containsKey(subjId)) {
				scanCount = _scanCount.get(subjId);
				
			} else {
				scanCount = new Integer(0);
				_scanCount.put(subjId, scanCount);
			}
			if (prevResult == null || prevResult.get("subjId") == null ||
					!prevResult.get("subjId").equals(result.get("subjId"))) {
				scanCount = 0;
				_scanCount.put(subjId, scanCount);
				scanTypeCounts.clear();
				scanTypeHighest.clear();
			}
			final String scanType = result.get("scanType");
			Double scanRating;
			try {
				scanRating = Double.valueOf(result.get("scanRating"));
			} catch (Exception e) {
				scanRating = null;
			}
			scanCount+=1;
			_scanCount.put(subjId, scanCount);
			if (scanTypeCounts.containsKey(scanType)) {
				Integer typeInt = scanTypeCounts.get(scanType);
				scanTypeCounts.put(scanType, typeInt+1);
				
			} else {
				scanTypeCounts.put(scanType, 1);
			}
			if (scanRating != null  && scanTypeHighest.containsKey(scanType)) {
				Double typeInt = scanTypeHighest.get(scanType);
				if (scanRating>typeInt) {
					scanTypeHighest.put(scanType, scanRating);
				}
			} else if (scanRating != null) {
				scanTypeHighest.put(scanType, scanRating);
			}
			
		}
		for (final Map<String, String> result : returnList) {
			final String scanType = result.get("scanType");
			final String subjId = result.get("subjId");
			final Map<String,Integer> scanTypeCounts = _scanTypeCounts.get(subjId);
			final Map<String,Double> scanTypeHighest = _scanTypeHighest.get(subjId);
			final Integer scanCount = _scanCount.get(subjId);
			Double scanRating;
			try {
				scanRating = Double.valueOf(result.get("scanRating"));
			} catch (Exception e) {
				scanRating = null;
			}
			result.put("scanCount", scanCount.toString());
			final Integer typeCount = scanTypeCounts.get(scanType);
			if (typeCount != null) {
				result.put("typeCount", scanTypeCounts.get(scanType).toString());
			}
			final Double typeHigh = scanTypeHighest.get(scanType);
			if (typeHigh != null && typeHigh.equals(scanRating)) {
				result.put("typeHighestRated", "Y");
				scanTypeHighest.remove(scanType);
			} else {
				result.put("typeHighestRated", "N");
			}
		}
		return returnList;
	}

	private static String getSiteValue(String projectId, String scanner) {
		String site = null;
		if (projectId.equals("CCF_HCA_ITK") || projectId.equals("CCF_HCD_ITK")) {
			if (scanner.contains("67056")) {
				site = "Harvard";
			} else if (scanner.contains("35177") || scanner.contains("166038")) {
				site = "WUSTL";
			} else if (scanner.contains("35343") || scanner.contains("35426")) { 
				site = "UCLA";
			} else if (scanner.contains("BAY4OC")) {
				site = "MGH";
			} else if (scanner.contains("TRIOC") || scanner.contains("166007")) {
				site = "UMN";
			} else {
				site = "UNKNOWN";
			}
		}
		return site;
	}
	

	private static Double getQualityRating(String note) {
		if (note == null) {
			return null;
		}
		String rating = (note != null) ? note.replaceAll("^.*[Ee][Kk][Rr]:","") : "";
		rating = rating.replaceAll("[^0-9. ]", "").trim();
		int space = rating.indexOf(' ');
		if (space>0) {
			rating = rating.substring(0, space);
		}
		try {
			return Double.valueOf(rating);
		} catch (Exception e) {
			return null;
		}
	}
	
	/*
	private static Double getQualityRating(XnatImagescandataI scan, XnatMrsessiondata exp) {
		if (scan == null) {
			return null;
		}
		final String note = scan.getNote();
		String rating = (note != null) ? note.replaceAll("^.*[Ee][Kk][Rr]:","") : "";
		rating = rating.replaceAll("[^0-9. ]", "").trim();
		int space = rating.indexOf(' ');
		if (space>0) {
			rating = rating.substring(0, space);
		}
		try {
			return Double.valueOf(rating);
		} catch (Exception e) {
			return null;
		}
	}
	*/
	
}
