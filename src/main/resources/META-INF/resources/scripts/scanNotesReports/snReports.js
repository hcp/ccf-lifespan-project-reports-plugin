
//# sourceURL=scanNotesReports/snReports.js

if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.snreports === 'undefined') {
	CCF.snreports = { 
		//populated : false
	};
}

if (typeof CCF.snreports.project === 'undefined' || CCF.snreports.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.snreports.project = queryParams.get("project");
}

CCF.snreports.buildColumnsFromTable = function(dataTable) {
	var columnVar = {};
	var data0 = dataTable[0];
	for (var col in data0) {
		if (data0.hasOwnProperty(col)) {
              		columnVar[col] = {
		                label: col,
		            	td: {'className': 'center'},
		                sort: false
              		};
		}
	}
	return columnVar;
}

CCF.snreports.initDataTable = function(project) {
	//console.log("INITIALIZE DATA TABLE");

	if (CCF.snreports.project == 'CCF_HCD_ITK') {
		$("[data-tab='hires-data-tab']").hide();
	}

	$("a[href='#sn-data-tab']").click(function() {
		setTimeout(function(){
			CCF.snreports.populateRowCount();
		},10);
	});

  $('#snDataReport-t1-contents').html("<div id='snd-t1-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='snDataReport-t1-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='snreports-t1-row-count-container''></div>");
  $('#snDataReport-hires-contents').html("<div id='snd-hires-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='snDataReport-hires-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='snreports-hires-row-count-container''></div>");
  $('#snDataReport-pcasl-contents').html("<div id='snd-pcasl-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='snDataReport-pcasl-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='snreports-pcasl-row-count-container''></div>");
  $('#snDataReport-fmri-contents').html("<div id='snd-fmri-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='snDataReport-fmri-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='snreports-fmri-row-count-container''></div>");
  $('#snDataReport-dmri-contents').html("<div id='snd-dmri-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='snDataReport-dmri-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='snreports-dmri-row-count-container''></div>");

// 
// Not allowing filtering because performance of the DataTable isn't good with as many rows are are displayed at the scan level
// 

    var zeroPad = function(num, places) {
	if (typeof num === 'undefined') {
		return "";
	}
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

  var columnVar = 
           {
              subject: {
                label: 'Subject',
                filter: false,
            	td: {'className': 'subject center rowcount'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.snreports.project + "/subjects/" + this.subjId + "' target='_blank'>" + this.subjLabel + "</a>";
                },
                sort: true
              },
              session: {
                label: 'Session',
                filter: false,
            	td: {'className': 'session center'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.snreports.project + "/subjects/" + this.subjId + "/experiments/" + this.expId +"' target='_blank'>" + this.expLabel + "</a>";
                },
                sort: true
              },
              expScanner: {
                label: 'Scanner',
                filter: false,
            	td: {'className': 'expScanner center'},
                sort: true
              },
              expSite: {
                label: 'Site',
                filter: false,
            	td: {'className': 'expSite center'},
                sort: true
              },
              subjAge: {
                label: 'Age',
                filter: false,
                apply: function() {
			// For numeric sorting
			return "<i style='display:none'>" + zeroPad(this.subjAge,2) + "</i>" + this.subjAge;
                },
            	td: {'className': 'subjAge center'},
                sort: true
              },
              scanId: {
                label: 'ScanId',
                filter: false,
                apply: function() {
			// For numeric sorting
			return "<i style='display:none'>" + zeroPad(this.scanId,4) + "</i>" + this.scanId;
                },
            	td: {'className': 'scanId center sn-highlight'},
                sort: true
              },
              scanType: {
                label: 'ScanType',
                filter: false,
            	td: {'className': 'scanType center sn-highlight'},
                sort: true
              },
              scanDesc: {
                label: 'ScanDesc',
                filter: false,
            	td: {'className': 'scanDesc center sn-highlight'},
                sort: true
              },
              scanQuality: {
                label: 'ScanQuality',
                filter: false,
            	td: {'className': 'scanQuality center'},
                sort: true
              },
              scanFrames: {
                label: 'Frames',
                filter: false,
                apply: function() {
			// For numeric sorting
			return "<i style='display:none'>" + zeroPad(this.scanFrames,4) + "</i>" + this.scanFrames;
                },
            	td: {'className': 'scanFrames center'},
                sort: true
              },
/*
              scanRating: {
                label: 'ScanRating',
                filter: true,
            	td: {'className': 'scanRating center'},
                sort: true
              },
              scanCount: {
                label: 'ScanCount',
                filter: true,
            	td: {'className': 'scanCount center'},
                sort: true
              },
              typeCount: {
                label: 'TypeCount',
                filter: true,
            	td: {'className': 'typeCount center'},
                sort: true
              },
              typeHighestRated: {
                label: 'HighestRated',
                filter: true,
            	td: {'className': 'typeHighestRated center'},
                sort: true
              },
*/
              scanNote: {
                label: 'ScanNote',
                filter: false,
            	td: {'className': 'scanNote left', 'style': 'min-width:280px;max-width:560px'},
                sort: true
              },
              sessionNote: {
                label: 'SessionNote',
                filter: false,
            	td: {'className': 'sessionNote left', 'style': 'min-width:280px;max-width:560px'},
                sort: true
              }
           }
  ;

// set up custom filter menus
function filterMenuElement(prop){
  //console.log(prop);
  if (!prop) return false;
  // call this function in context of the table
  var $pipelineTable = $(this);
  //console.log($pipelineTable);
  var FILTERCLASS = 'filter-' + prop;
  //console.log(FILTERCLASS);
  return {
    id: 'pcp-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        $("td." + prop).each(function() {
          if ($(this).hasClass("filter")) {
              return true;
          }
          var $row = $(this).closest('tr');
           //console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })

	CCF.snreports.populateRowCount();

      }
    }
  };
}


/*
  XNAT.xhr.getJSON({
     url: '/xapi/lifespanReports/project/' + CCF.snreports.project +
          '/lastCacheUpdate',
     success: function(data) {
        CCF.snreports.dataLastUpdateTime = data;
*/

	  XNAT.xhr.getJSON({
	     url: '/xapi/scanNotesReports/project/' + CCF.snreports.project +
	          '/dataTable',
	     success: function(data) {
	        CCF.snreports.snDataTableData_t1 = [];
	        CCF.snreports.snDataTableData_hires = [];
	        CCF.snreports.snDataTableData_pcasl = [];
	        CCF.snreports.snDataTableData_fmri = [];
	        CCF.snreports.snDataTableData_dmri = [];
	        CCF.snreports.snDataTableData_nomatches = [];
		for (var i=0; i<data.length; i++) {
			var type = data[i]["scanType"];
			if (type.toUpperCase().includes("PCASL")) {
				CCF.snreports.snDataTableData_pcasl.push(data[i]);
			} else if (type.toUpperCase().includes("HIRES")) {
				CCF.snreports.snDataTableData_hires.push(data[i]);
			} else if ($.isArray(type.toUpperCase().match(/T[12]W/))) {
				CCF.snreports.snDataTableData_t1.push(data[i]);
			} else if (type.toUpperCase().includes("FMRI")) {
				CCF.snreports.snDataTableData_fmri.push(data[i]);
			} else if (type.toUpperCase().includes("DMRI")) {
				CCF.snreports.snDataTableData_dmri.push(data[i]);
			} else {
				CCF.snreports.snDataTableData_nomatches.push(data[i]);
			}
		} 
		if (CCF.snreports.snDataTableData_nomatches.length>0) {
			console.log("WARNING:  Not all returned records were matched to a scanType");
			console.log(CCF.snreports.snDataTableData);
		}

	        XNAT.table.dataTable(CCF.snreports.snDataTableData_t1, {
	           table: {
	              id: 'ls-sn-t1-datatable',
	              className: 'ls-sn-t1-table sn-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#snDataReport-t1-table'))
	        $("#snDataReport-t1-table").find(".data-table-wrapper").css("display","table");
	     	$("#snd-t1-building-div").html("<div style='width:100%;text-align:right'><a id='sn-t1-download-csv' download='ScanNotesDataTableReport_t1t2.csv' onclick='CCF.snreports.doDownload(\"t1\")' type='text/csv'>Download CSV</a></div>");

	        XNAT.table.dataTable(CCF.snreports.snDataTableData_hires, {
	           table: {
	              id: 'ls-sn-hires-datatable',
	              className: 'ls-sn-hires-table sn-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#snDataReport-hires-table'))
	        $("#snDataReport-hires-table").find(".data-table-wrapper").css("display","table");
	     	$("#snd-hires-building-div").html("<div style='width:100%;text-align:right'><a id='sn-hires-download-csv' download='ScanNotesDataTableReport_hires.csv' onclick='CCF.snreports.doDownload(\"hires\")' type='text/csv'>Download CSV</a></div>");

	        XNAT.table.dataTable(CCF.snreports.snDataTableData_pcasl, {
	           table: {
	              id: 'ls-sn-pcasl-datatable',
	              className: 'ls-sn-pcasl-table sn-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#snDataReport-pcasl-table'))
	        $("#snDataReport-pcasl-table").find(".data-table-wrapper").css("display","table");
	     	$("#snd-pcasl-building-div").html("<div style='width:100%;text-align:right'><a id='sn-pcasl-download-csv' download='ScanNotesDataTableReport_pcasl.csv' onclick='CCF.snreports.doDownload(\"pcasl\")' type='text/csv'>Download CSV</a></div>");

	        XNAT.table.dataTable(CCF.snreports.snDataTableData_fmri, {
	           table: {
	              id: 'ls-sn-fmri-datatable',
	              className: 'ls-sn-fmri-table sn-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#snDataReport-fmri-table'))
	        $("#snDataReport-fmri-table").find(".data-table-wrapper").css("display","table");
	     	$("#snd-fmri-building-div").html("<div style='width:100%;text-align:right'><a id='sn-fmri-download-csv' download='ScanNotesDataTableReport_fmri.csv' onclick='CCF.snreports.doDownload(\"fmri\")' type='text/csv'>Download CSV</a></div>");

	        XNAT.table.dataTable(CCF.snreports.snDataTableData_dmri, {
	           table: {
	              id: 'ls-sn-dmri-datatable',
	              className: 'ls-sn-dmri-table sn-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#snDataReport-dmri-table'))
	        $("#snDataReport-dmri-table").find(".data-table-wrapper").css("display","table");
	     	$("#snd-dmri-building-div").html("<div style='width:100%;text-align:right'><a id='sn-dmri-download-csv' download='ScanNotesDataTableReport_dmri.csv' onclick='CCF.snreports.doDownload(\"dmri\")' type='text/csv'>Download CSV</a></div>");

	        setTimeout(function(){
	    		$('.sn-highlight').on("click",function() {
				$(this).closest('tr').find('td').each(function(){
					if ($(this).css("background-color").toString().indexOf('238')>=0) {
						$(this).css("background-color","");
					} else {
						$(this).css("background-color","rgb(238,238,238)");
					}
				});
			});
	        },100);

/*
		// 
		// Commenting this out because performance of the DataTable isn't good with as many rows are are displayed at the scan level
		// 
	        setTimeout(function(){
	        	CCF.snreports.populateRowCount("t1");
	        },100);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.snreports.populateRowCount("t1");
	           },50)
	        });
	        setTimeout(function(){
	        	CCF.snreports.populateRowCount("hires");
	        },150);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.snreports.populateRowCount("hires");
	           },50)
	        });
	        setTimeout(function(){
	        	CCF.snreports.populateRowCount("pcasl");
	        },200);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.snreports.populateRowCount("pcasl");
	           },50)
	        });
	        setTimeout(function(){
	        	CCF.snreports.populateRowCount("fmri");
	        },250);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.snreports.populateRowCount("fmri");
	           },50)
	        });
	        setTimeout(function(){
	        	CCF.snreports.populateRowCount("dmri");
	        },300);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.snreports.populateRowCount("dmri");
	           },50)
	        });
*/
	       	CCF.snreports.displayLastUpdated("t1");
	       	CCF.snreports.displayLastUpdated("hires");
	       	CCF.snreports.displayLastUpdated("pcasl");
	       	CCF.snreports.displayLastUpdated("fmri");
	       	CCF.snreports.displayLastUpdated("dmri");
	
	     },
	     failure: function(data) {
	     	$("#snd-t1-building-div").html("<h3>Failed to build report</h3>");
	     	$("#snd-hires-building-div").html("<h3>Failed to build report</h3>");
	     	$("#snd-pcasl-building-div").html("<h3>Failed to build report</h3>");
	     	$("#snd-fmri-building-div").html("<h3>Failed to build report</h3>");
	     	$("#snd-dmri-building-div").html("<h3>Failed to build report</h3>");
	     }
	  });

/*
     },
     failure: function(data) {
     	$("#snd-t1-building-div").html("<h3>Failed to build report</h3>");
     	$("#snd-hires-building-div").html("<h3>Failed to build report</h3>");
     	$("#snd-pcasl-building-div").html("<h3>Failed to build report</h3>");
     	$("#snd-fmri-building-div").html("<h3>Failed to build report</h3>");
     	$("#snd-dmri-building-div").html("<h3>Failed to build report</h3>");
     }
  });
*/

}

CCF.snreports.populateRowCount = function(type) {
    if ($("#snd-" + type + "-building-div").find('img').length<1) {
    	$('#snreports-' + type + '-row-count-container').html("<em>" + ($('.rowcount').closest('tr').filter(":visible")).length + " rows match query filters.  " +
		"NOTE: Click on any non-link row to highlight that row for viewing.<br>" +
		"</em>");

		/*
		"The data used to generate this report was last updated at " + (new Date(CCF.snreports.dataLastUpdateTime).toLocaleString()) +"</em>");
		*/
    } else {
    	$('#snreports-' + type + '-row-count-container').empty();
    }
}

CCF.snreports.displayLastUpdated = function(type) {
    if ($("#snd-" + type + "-building-div").find('img').length<1) {
	if ($.isArray(type.toUpperCase().match(/MRI/))) {
    		$('#snreports-' + type + '-row-count-container').html(
			"");
			/*
			"<em>NOTE:  Only scans with scan notes are included in this report.  The data used to generate this report was last updated at " +
			(new Date(CCF.snreports.dataLastUpdateTime).toLocaleString()) +"</em>");
			*/
	} else {
    		$('#snreports-' + type + '-row-count-container').html(
			"");
			/*
			"<em>The data used to generate this report was last updated at " +
			 (new Date(CCF.snreports.dataLastUpdateTime).toLocaleString()) +"</em>");
			*/
	}
    } else {
    	$('#snreports-' + type + '-row-count-container').empty();
    }
}


CCF.snreports.doDownload = function(type) {
	var csv = CCF.snreports.generateCSV(type);
	var data = new Blob([csv]);
	var ele = document.getElementById("sn-" + type + "-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.snreports.generateCSV = function(type) {

    var dta;

	if (type == 't1') {
		dta = CCF.snreports.snDataTableData_t1;
	} else if (type == 'hires') {
		dta = CCF.snreports.snDataTableData_hires;
	} else if (type == 'pcasl') {
		dta = CCF.snreports.snDataTableData_pcasl;
	} else if (type == 'fmri') {
		dta = CCF.snreports.snDataTableData_fmri;
	} else if (type == 'dmri') {
		dta = CCF.snreports.snDataTableData_dmri;
	}

    var keys = Object.keys(dta[0]);

    var result = keys.join(",") + "\n";

    dta.forEach(function(obj){
        keys.forEach(function(k, ix){
            if (typeof obj[k] === 'undefined') {
                obj[k] = "";
            }
            if (ix) result += ",";
            result += ((obj[k].toString().indexOf(",")<0 && obj[k].toString().indexOf(" ")<0) ? obj[k].toString().replace(/\r?\n/g,'') : "\"" + obj[k].toString().replace(/\r?\n/g,'').replace('"','\\"') + "\"");
        });
        result += "\n";
    });

    return result;

}

