
//# sourceURL=lifespanReports/ccfSessionCountsReports.js

if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.sitereports === 'undefined') {
	CCF.sitereports = { 
		//populated : false
	};
}

CCF.sitereports.initSessionCounts = function(project) {
	console.log("INITIALIZE SESSION COUNTS!!!!");
 
  $('#ccfSessionCounts-contents').html("<div id='s-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='ccfSessionCounts-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>");

  var columnVar =
           {
              ProjectID: {
                label: 'ProjectID',
		filter: true,
            	td: {'className': 'ProjectID center'},
                sort: true
              },
              ProjectType: {
                label: 'ProjectType',
		filter: true,
            	td: {'className': 'ProjectType center'},
                sort: true
              },
              ProjectName: {
                label: 'ProjectName',
		filter: true,
            	td: {'className': 'ProjectName center'},
                sort: true
              },
              ProjectPI: {
                label: 'ProjectPI',
		filter: true,
            	td: {'className': 'ProjectPI center'},
                sort: true
              },
              Subjects: {
                label: 'Subjects',
            	td: {'className': 'center'},
                apply: function() {
                  var tempVar = ("      " + this.Subjects);
                  tempVar = tempVar.substring(tempVar.length-6);
                  return tempVar;
		
                },
                sort: true
              },
              Sessions: {
                label: 'Sessions',
            	td: {'className': 'center'},
                apply: function() {
                  var tempVar = ("      " + this.Sessions);
                  tempVar = tempVar.substring(tempVar.length-6);
                  return tempVar;
                },
                sort: true
              }
           };

  XNAT.xhr.getJSON({
     url: '/xapi/lifespanReports/subjectSessionCountsTable',
     success: function(data) {
	for (var i=0;i<data.length;i++) {
           data[i]["projectid"] = data[i].ProjectID;
        }
	CCF.sitereports.sessionCounts = data;
	
        XNAT.table.dataTable(CCF.sitereports.sessionCounts, {
           table: {
              id: 'ccf-subjectSessionCounts',
              className: 'ccf-sessioncounts-table'
           },
           width: 'fit-content',
           overflowY: 'hidden',
           overflowX: 'hidden',
           columns: columnVar
        }).render($('#ccfSessionCounts-table'))
        $("#ccfSessionCounts-table").find(".data-table-wrapper").css("display","table");
     	$("#s-building-div").remove();
     },
     failure: function(data) {
     	$("#s-building-div").html("<h3>Failed to build report</h3>");
     }
  });

	$("li.tab[data-tab='ccf-site-reports-tab']").click();

}

