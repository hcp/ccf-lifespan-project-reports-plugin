
//# sourceURL=lifespanReports/qcReports.js


if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.lsreports === 'undefined') {
	CCF.lsreports = { 
		//populated : false
	};
}

if (typeof CCF.lsreports.project === 'undefined' || CCF.lsreports.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.lsreports.project = queryParams.get("project");
}

CCF.lsreports.initSummary = function(project) {
	//console.log("INITIALIZE SUMMARY REPORT");

  var columnVar =
           {
              site: {
                label: 'Site',
            	td: {'className': 'center'},
                sort: false
              },
              T1U_N: {
                label: 'Subjects',
            	td: {'className': 'center'},
                sort: false
              },
              COMPLETE_MEAN: {
                label: 'Complete',
            	td: {'className': 'center'},
                sort: false
              },
              age_MEAN: {
                label: 'Age',
            	td: {'className': 'center'},
                sort: false
              },
              T1U_MEAN: {
                label: 'T1_Releasable',
            	td: {'className': 'center'},
                sort: false
              },
              T1R_MEAN: {
                label: 'T1_Rating',
            	td: {'className': 'center'},
                sort: false
              },
              T2U_MEAN: {
                label: 'T2_Releasable',
            	td: {'className': 'center'},
                sort: false
              },
              T2R_MEAN: {
                label: 'T2_Rating',
            	td: {'className': 'center'},
                sort: false
              },
              REST_AP_1_REL_RMS_MEAN: {
                label: 'RestAP1_RelRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_PA_1_REL_RMS_MEAN: {
                label: 'RestPA1_RelRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_AP_2_REL_RMS_MEAN: {
                label: 'RestAP2_RelRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_PA_2_REL_RMS_MEAN: {
                label: 'RestPA2_RelRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_AP_1_ABS_RMS_MEAN: {
                label: 'RestAP1_AbsRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_PA_1_ABS_RMS_MEAN: {
                label: 'RestPA1_AbsRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_AP_2_ABS_RMS_MEAN: {
                label: 'RestAP2_AbsRMS',
            	td: {'className': 'center'},
                sort: false
              },
              REST_PA_2_ABS_RMS_MEAN: {
                label: 'RestPA2_AbsRMS',
            	td: {'className': 'center'},
                sort: false
              },
           };

        XNAT.table.dataTable(CCF.lsreports.summaryData, {
           table: {
              id: 'ls-qc-summarytable',
              className: 'ls-qc-table'
           },
           width: 'fit-content',
           overflowY: 'hidden',
           overflowX: 'hidden',
           columns: columnVar
        }).render($('#qcSummaryReport-table'))
	/*
     	$("#qcSummaryReport-contents").append("<em>The data used to generate this report was last updated at " + (new Date(CCF.lsreports.dataLastUpdateTime).toLocaleString()) +"</em>");
	*/
        $("#qcSummaryReport-table").find(".data-table-wrapper").css("display","table");
     	$("#s-building-div").remove();
	CCF.lsreports.initCharts(CCF.lsreports.project);
}

CCF.lsreports.initDataTable = function(project) {
	//console.log("INITIALIZE DATA TABLE");

	$("a[href='#qc-data-tab']").click(function() {
		setTimeout(function(){
			CCF.lsreports.populateRowCount();
		},10);
	});

  $('#qcDataReport-contents').html("<div id='building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='qcDataReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='lsreports-row-count-container''></div>");
  $('#qcSummaryReport-contents').html("<div id='s-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='qcSummaryReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>");
  $('#qcChartsReport-contents').html("<div id='s-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Charts.</div>" +
         "<div id='qcChartReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>");

	$("li.tab[data-tab='qc-summary-tab']").click();

  var columVar;
  if (CCF.lsreports.project == "CCF_HCD_ITK") {
    columnVar = 
           {
              date: {
                label: 'Date',
                filter: true,
            	td: {'className': 'date center highlight' },
                apply: function() {
                  return "<nobr>" + this.date + "</nobr>";
                },
                sort: true
              },
              subject: {
                label: 'Subject',
                filter: true,
            	td: {'className': 'subject center'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.lsreports.project + "/subjects/" + this.subject + "' target='_blank'>" + this.subject + "</a>";
                },
                sort: true
              },
              age: {
                label: 'Age',
                filter: true,
            	td: {'className': 'age center highlight'},
                sort: true
              },
              scanner: {
                label: 'Scanner',
                filter: true,
            	td: {'className': 'scanner center highlight'},
                sort: true
              },
              site: {
                label: 'Site',
                filter: true,
            	td: {'className': 'site center highlight'},
                sort: true
              },
              T1U: {
                label: 'T1_Releasable',
            	td: {'className': 'T1U center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'T1U')
                   }).element])
                 },
                sort: true
              },
              T1R: {
                label: 'T1_Rating',
                filter: true,
            	td: {'className': 'T1R center highlight'},
                sort: true
              },
              T2U: {
                label: 'T2_Releasable',
            	td: {'className': 'T2U center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'T2U')
                   }).element])
                 },
                sort: true
              },
              T2R: {
                label: 'T2_Rating',
                filter: true,
            	td: {'className': 'T2R center highlight'},
                sort: true
              },
              REST_AP_1: {
                label: 'REST_AP_1',
            	td: {'className': 'REST_AP_1 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_AP_1')
                   }).element])
                 },
                sort: true
              },
              REST_AP_1_REL_RMS: {
                label: 'REST_AP_1_REL_RMS',
                filter: true,
            	td: {'className': 'REST_AP_1_REL_RMS center highlight'},
                sort: true
              },
              REST_AP_1_ABS_RMS: {
                label: 'REST_AP_1_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_AP_1_ABS_RMS center highlight'},
                sort: true
              },
              REST_PA_1: {
                label: 'REST_PA_1',
            	td: {'className': 'REST_PA_1 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_PA_1')
                   }).element])
                 },
                sort: true
              },
              REST_PA_1_REL_RMS: {
                label: 'REST_PA_1_REL_RMS',
                filter: true,
            	td: {'className': 'REST_PA_1_REL_RMS center highlight'},
                sort: true
              },
              REST_PA_1_ABS_RMS: {
                label: 'REST_PA_1_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_PA_1_ABS_RMS center highlight'},
                sort: true
              },
              GUESS_AP: {
                label: 'GUESS_AP',
            	td: {'className': 'GUESS_AP center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'GUESS_AP')
                   }).element])
                 },
                sort: true
              },
              GUESS_PA: {
                label: 'GUESS_PA',
            	td: {'className': 'GUESS_PA highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'GUESS_PA')
                   }).element])
                 },
                sort: true
              },
              CARIT_AP: {
                label: 'CARIT_AP',
            	td: {'className': 'CARIT_AP center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'CARIT_AP')
                   }).element])
                 },
                sort: true
              },
              CARIT_PA: {
                label: 'CARIT_PA',
            	td: {'className': 'CARIT_PA highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'CARIT_PA')
                   }).element])
                 },
                sort: true
              },
              EMOT: {
                label: 'EMOT',
            	td: {'className': 'EMOT center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'EMOT')
                   }).element])
                 },
                sort: true
              },
              MBPCASL: {
                label: 'MBPCASL',
            	td: {'className': 'MBPCASL center highlight'},
                apply: function() {
                  	if (this.MBPCASL=='1') {
				return '1-HiRes';
			} else if (this.MBPCASL=='0.5') {
				return '1-LoRes';
			} else {
				 return this.MBPCASL; 
			};
                },
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes-HiRes', value: '1-HiRes' },
                       { label: 'Yes-LoRes', value: '1-LoRes' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'MBPCASL')
                   }).element])
                 },
                sort: true
              },
              REST_AP_2: {
                label: 'REST_AP_2',
            	td: {'className': 'REST_AP_2 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_AP_2')
                   }).element])
                 },
                sort: true
              },
              REST_AP_2_REL_RMS: {
                label: 'REST_AP_2_REL_RMS',
                filter: true,
            	td: {'className': 'REST_AP_2_REL_RMS center highlight'},
                sort: true
              },
              REST_AP_2_ABS_RMS: {
                label: 'REST_AP_2_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_AP_2_ABS_RMS center highlight'},
                sort: true
              },
              REST_PA_2: {
                label: 'REST_PA_2',
            	td: {'className': 'REST_PA_2 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_PA_2')
                   }).element])
                 },
                sort: true
              },
              REST_PA_2_REL_RMS: {
                label: 'REST_PA_2_REL_RMS',
                filter: true,
            	td: {'className': 'REST_PA_2_REL_RMS center highlight'},
                sort: true
              },
              REST_PA_2_ABS_RMS: {
                label: 'REST_PA_2_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_PA_2_ABS_RMS center highlight'},
                sort: true
              },
              DMRI: {
                label: 'DMRI',
            	td: {'className': 'DMRI center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'DMRI')
                   }).element])
                 },
                sort: true
              },
              COMPLETE: {
                label: 'Complete',
            	td: {'className': 'COMPLETE center rowcount highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'COMPLETE')
                   }).element])
                 },
                sort: true
              }
           }
  } else if (CCF.lsreports.project == "CCF_HCA_ITK") {
    columnVar = 
           {
              date: {
                label: 'Date',
                filter: true,
            	td: {'className': 'date center highlight' },
                apply: function() {
                  return "<nobr>" + this.date + "</nobr>";
                },
                sort: true
              },
              subject: {
                label: 'Subject',
                filter: true,
            	td: {'className': 'subject center'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.lsreports.project + "/subjects/" + this.subject + "' target='_blank'>" + this.subject + "</a>";
                },
                sort: true
              },
              age: {
                label: 'Age',
                filter: true,
            	td: {'className': 'age center highlight'},
                sort: true
              },
              scanner: {
                label: 'Scanner',
                filter: true,
            	td: {'className': 'scanner center highlight'},
                sort: true
              },
              site: {
                label: 'Site',
                filter: true,
            	td: {'className': 'site center highlight'},
                sort: true
              },
              T1U: {
                label: 'T1_Releasable',
            	td: {'className': 'T1U center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'T1U')
                   }).element])
                 },
                sort: true
              },
              T1R: {
                label: 'T1_Rating',
                filter: true,
            	td: {'className': 'T1R center highlight'},
                sort: true
              },
              T2U: {
                label: 'T2_Releasable',
            	td: {'className': 'T2U center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'T2U')
                   }).element])
                 },
                sort: true
              },
              T2R: {
                label: 'T2_Rating',
                filter: true,
            	td: {'className': 'T2R center highlight'},
                sort: true
              },
              REST_AP_1: {
                label: 'REST_AP_1',
            	td: {'className': 'REST_AP_1 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_AP_1')
                   }).element])
                 },
                sort: true
              },
              REST_AP_1_REL_RMS: {
                label: 'REST_AP_1_REL_RMS',
                filter: true,
            	td: {'className': 'REST_AP_1_REL_RMS center highlight'},
                sort: true
              },
              REST_AP_1_ABS_RMS: {
                label: 'REST_AP_1_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_AP_1_ABS_RMS center highlight'},
                sort: true
              },
              REST_PA_1: {
                label: 'REST_PA_1',
            	td: {'className': 'REST_PA_1 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_PA_1')
                   }).element])
                 },
                sort: true
              },
              REST_PA_1_REL_RMS: {
                label: 'REST_PA_1_REL_RMS',
                filter: true,
            	td: {'className': 'REST_PA_1_REL_RMS center highlight'},
                sort: true
              },
              REST_PA_1_ABS_RMS: {
                label: 'REST_PA_1_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_PA_1_ABS_RMS center highlight'},
                sort: true
              },
              VISMOTOR: {
                label: 'VISMOTOR',
            	td: {'className': 'VISMOTOR center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'VISMOTOR')
                   }).element])
                 },
                sort: true
              },
              CARIT: {
                label: 'CARIT',
            	td: {'className': 'CARIT center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'CARIT')
                   }).element])
                 },
                sort: true
              },
              FACENAME: {
                label: 'FACENAME',
            	td: {'className': 'FACENAME center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'FACENAME')
                   }).element])
                 },
                sort: true
              },
              MBPCASL: {
                label: 'MBPCASLhr',
            	td: {'className': 'MBPCASL center highlight'},
                apply: function() {
                  	if (this.MBPCASL=='1') {
				return '1-HiRes';
			} else if (this.MBPCASL=='0.5') {
				return '1-LoRes';
			} else {
				 return this.MBPCASL; 
			};
                },
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes-HiRes', value: '1-HiRes' },
                       { label: 'Yes-LoRes', value: '1-LoRes' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'MBPCASL')
                   }).element])
                 },
                sort: true
              },
              REST_AP_2: {
                label: 'REST_AP_2',
            	td: {'className': 'REST_AP_2 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_AP_2')
                   }).element])
                 },
                sort: true
              },
              REST_AP_2_REL_RMS: {
                label: 'REST_AP_2_REL_RMS',
                filter: true,
            	td: {'className': 'REST_AP_2_REL_RMS center highlight'},
                sort: true
              },
              REST_AP_2_ABS_RMS: {
                label: 'REST_AP_2_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_AP_2_ABS_RMS center highlight'},
                sort: true
              },
              REST_PA_2: {
                label: 'REST_PA_2',
            	td: {'className': 'REST_PA_2 center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'REST_PA_2')
                   }).element])
                 },
                sort: true
              },
              REST_PA_2_REL_RMS: {
                label: 'REST_PA_2_REL_RMS',
                filter: true,
            	td: {'className': 'REST_PA_2_REL_RMS center highlight'},
                sort: true
              },
              REST_PA_2_ABS_RMS: {
                label: 'REST_PA_2_ABS_RMS',
                filter: true,
            	td: {'className': 'REST_PA_2_ABS_RMS center highlight'},
                sort: true
              },
              DMRI: {
                label: 'DMRI',
            	td: {'className': 'DMRI center highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'DMRI')
                   }).element])
                 },
                sort: true
              },
              COMPLETE: {
                label: 'Complete',
            	td: {'className': 'COMPLETE center rowcount highlight'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'All', value: 'all' },
                       { label: 'Yes', value: '1' },
                       { label: 'No', value: '0' }
                     ],
                     element: filterMenuElement.call(table, 'COMPLETE')
                   }).element])
                 },
                sort: true
              }
           }
  }

// set up custom filter menus
function filterMenuElement(prop){
  //console.log(prop);
  if (!prop) return false;
  // call this function in context of the table
  var $pipelineTable = $(this);
  //console.log($pipelineTable);
  var FILTERCLASS = 'filter-' + prop;
  //console.log(FILTERCLASS);
  return {
    id: 'pcp-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        $("td." + prop).each(function() {
          if ($(this).hasClass("filter")) {
              return true;
          }
          var $row = $(this).closest('tr');
           //console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })

	CCF.lsreports.populateRowCount();

      }
    }
  };
}







/*
  XNAT.xhr.getJSON({
     url: '/xapi/lifespanReports/project/' + CCF.lsreports.project +
          '/lastCacheUpdate',
     success: function(data) {
        CCF.lsreports.dataLastUpdateTime = data;
*/


	  XNAT.xhr.getJSON({
	
	     url: '/xapi/lifespanReports/project/' + CCF.lsreports.project +
	          '/qcTables',
	     success: function(data) {

		  CCF.lsreports.qcDataTableData = data["qcDataTable"];
		  CCF.lsreports.summaryData = data["qcSummaryTable"];

	        XNAT.table.dataTable(CCF.lsreports.qcDataTableData, {
	           table: {
	              id: 'ls-qc-datatable',
	              className: 'ls-qc-table highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#qcDataReport-table'))
	        $("#qcDataReport-table").find(".data-table-wrapper").css("display","table");
	     	$("#building-div").html("<div style='width:100%;text-align:right'><a id='qc-download-csv' download='QCDataTableReport.csv' onclick='CCF.lsreports.doDownload()' type='text/csv'>Download CSV</a></div>");
        	CCF.lsreports.initSummary();
	        setTimeout(function(){
	        	CCF.lsreports.populateRowCount();
	        },50);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.lsreports.populateRowCount();
	           },50)
	        });
	        setTimeout(function(){
	    		$('.highlight').on("click",function() {
				$(this).closest('tr').find('td').each(function(){
					if ($(this).css("background-color").toString().indexOf('228')>=0) {
						$(this).css("background-color","");
					} else {
						$(this).css("background-color","rgb(228,228,228)");
					}
				});
			});
	        },100);

	     },
	     failure: function(data) {
     		$("#s-building-div").html("<h3>Failed to build report</h3>");
	     	$("#sqd-building-div").html("<h3>Failed to build report</h3>");
	     }
	  });

/*
     },
     failure: function(data) {
     	$("#building-div").html("<h3>Failed to build report</h3>");
     }
  });

*/


/*
  XNAT.table.dataTable([], {
     url: '/xapi/lifespanReports/project/' + CCF.lsreports.project +
          '/qcDataTable',
     table: {
        id: 'ls-qc-datatable',
        className: 'ls-qc-table'
     },
     overflowY: 'auto',
     overflowX: 'auto',
     after: removeBuildingDiv,
     columns: {
        date: {
          label: 'Date',
          filter: true,
          sort: true
        },
        subject: {
          label: 'Subject',
          sort: true
        },
        age: {
          label: 'Age',
          sort: true
        },
        scanner: {
          label: 'Scanner',
          sort: true
        },
        site: {
          label: 'Site',
          filter: true,
          sort: true
        }
     }
  }).render($('#qcDataReport-contents'))
*/

}


CCF.lsreports.populateRowCount = function() {
    if ($("#building-div").find('img').length<1) {
    	$('#lsreports-row-count-container').html("<em>" + ($('.rowcount').closest('tr').filter(":visible")).length + " rows match query filters.  " +
		"NOTE: Click on any non-link row to highlight that row for viewing.<br>" +
		"</em>");
		/*
		"The data used to generate this report was last updated at " + (new Date(CCF.lsreports.dataLastUpdateTime).toLocaleString()) +"</em>");
		*/
    } else {
    	$('#lsreports-row-count-container').empty();
    }
}



CCF.lsreports.initCharts = function(project) {

	$("a[href='#qc-charts-tab']").click(function() {
		setTimeout(function(){
			var chartDivs = $(".qc-chart");
			if (chartDivs.length>0) {
				return;
			}	
			CCF.lsreports.drawCharts(project);
		},25);
	});

	$( window ).resize(function() {
		if ($("li[data-tab='qc-charts-tab'].active").length>0) {
			return;
		}
  		$('#qcChartsReport-contents').html("<div id='s-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Charts.</div>" +
         		"<div id='qcChartReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>");
	});

}

CCF.lsreports.drawCharts = function(project) {
	//console.log("INITIALIZE CHARTS");
	//$("#qcChartsReport-contents").html("<h3>QC CHARTS!!!</h3>");
	$("#qcChartsReport-contents").html("");
	var data = CCF.lsreports.summaryData;
	//console.log(project);
	var varList = ((project=="CCF_HCD_ITK") ? 
		[ "COMPLETE_N", "age_MEAN", "COMPLETE_MEAN", "T1U_MEAN", "T1R_MEAN", "T2U_MEAN", "T2R_MEAN", "REST_AP_1_MEAN", "REST_PA_1_MEAN", "REST_AP_2_MEAN", "REST_PA_2_MEAN", "GUESS_AP_MEAN", "GUESS_PA_MEAN", "CARIT_PA_MEAN", "CARIT_AP_MEAN", "EMOT_MEAN", "MBPCASL_MEAN", "DMRI_MEAN", "REST_AP_1_REL_RMS_MEAN", "REST_PA_1_REL_RMS_MEAN", "REST_AP_2_REL_RMS_MEAN", "REST_PA_2_REL_RMS_MEAN", "REST_AP_1_ABS_RMS_MEAN", "REST_PA_1_ABS_RMS_MEAN", "REST_AP_2_ABS_RMS_MEAN", "REST_PA_2_ABS_RMS_MEAN" ] :
		[ "COMPLETE_N", "age_MEAN", "COMPLETE_MEAN", "T1U_MEAN", "T1R_MEAN", "T2U_MEAN", "T2R_MEAN", "REST_AP_1_MEAN", "REST_PA_1_MEAN", "REST_AP_2_MEAN", "REST_PA_2_MEAN", "VISMOTOR_MEAN", "CARIT_MEAN", "FACENAME_MEAN", "MBPCASL_MEAN", "DMRI_MEAN", "REST_AP_1_REL_RMS_MEAN", "REST_PA_1_REL_RMS_MEAN", "REST_AP_2_REL_RMS_MEAN", "REST_PA_2_REL_RMS_MEAN", "REST_AP_1_ABS_RMS_MEAN", "REST_PA_1_ABS_RMS_MEAN", "REST_AP_2_ABS_RMS_MEAN", "REST_PA_2_ABS_RMS_MEAN" ]);
	var varLabel = ((project=="CCF_HCD_ITK") ? 
		[ "Subjects", "Age", "Complete", "T1_Releasable", "T1_Rating", "T2_Releasable", "T2_Rating", "REST_AP_1", "REST_PA_1", "REST_AP_2", "REST_PA_2", "GUESS_AP", "GUESS_PA", "CARIT_PA", "CARIT_AP", "EMOT", "MBPCASL", "DMRI", "RestAP1_RelRMS", "RestPA1_RelRMS", "RestAP2_RelRMS", "RestPA2_RelRMS", "RestAP1_AbsRMS", "RestPA1_AbsRMS", "RestAP2_AbsRMS", "RestPA2_AbsRMS" ] :
		[ "Subjects", "Age", "Complete", "T1_Releasable", "T1_Rating", "T2_Releasable", "T2_Rating", "REST_AP_1", "REST_PA_1", "REST_AP_2", "REST_PA_2", "VISMOTOR", "CARIT", "FACENAME", "MBPCASL", "DMRI", "RestAP1_RelRMS", "RestPA1_RelRMS", "RestAP2_RelRMS", "RestPA2_RelRMS", "RestAP1_AbsRMS", "RestPA1_AbsRMS", "RestAP2_AbsRMS", "RestPA2_AbsRMS" ]) ;

	for (var i=0; i<varList.length; i++) {
		var doContinue = true;
		for (var j=0; j<data.length; j++) {
			var thisVar = data[j][varList[i]];
			if (typeof thisVar === "undefined" || thisVar === "") {
				doContinue = false;
			}
		}
		if (!doContinue) {
			continue;
		}
		$("#qcChartsReport-contents").append("<div id='c3-qc-chart-" + varList[i] + "' class='qc-chart' style='width:340px;height:320px;margin-bottom:40px;float:left;'></div>");
		var chart_columns = [];
		for (var j=0; j<data.length; j++) {
			if (data[j].site == "_TOTAL_") {
				continue;
			}
			var col = [];
			col.push(data[j].site);
			col.push(parseFloat(data[j][varList[i]]));
			chart_columns.push(col);
		}

        var colori=0;
        var colorva = [];
        var coloria = [];
        var colorobj = {};
        var color = d3.interpolateRgb("#D1D1D1","#084fab");
        for (var k=0;k<chart_columns.length;k++) {
            colorobj[chart_columns[k][0]] = color(k/chart_columns.length);
        }


        var chart = c3.generate({
            bindto: '#c3-qc-chart-' + varList[i],
            data: {
                    columns: chart_columns,
                    //type: (function() { return (chart_columns.length<5) ? "pie: { label: { show: false }}" : "bar"; })(),
                    type: "bar",
/*
                    type: (function() { return (chart_columns.length<5) ? "pie" : "bar"; })(),
                    onclick: function (d,i) { filterFromChart(d,i,$('#' + s_div_table_id).data("statsresults")) },
                    onmouseover: function (d, i) { console.log("onmouseover" + "\n" + d + "\n" + i) },
                    onmouseout: function (d, i) { console.log("onmouseout" + "\n" + d + "\n" + i); },
                    onhover: function (d, i) { console.log("onhover" + "\n" + d + "\n" + i); },
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); },
*/
                    colors: colorobj,
            },
            interaction: {
                enabled: true
            },
            tooltip: {
                show: true
            },
            pie: {
                expand: false
                //label: { show: true, format: function(value,ratio) { return value + '(' + (ratio*100).toFixed(1) + ')%' } }
            }, 
            //onmouseover: function (d, i) { console.log("MOUSEOVER!!!"); },
            axis: {
                // TO DO:  Currently can't seem to control the display of the X-axis for bar charts (ticks)
                x: {
                        show: true,
                        categories: [],
                        tick: {  format: function(x) {
						return "";
					}
			}
                    },
                y: {
                        //label: { text: varList[i] , position: 'outer-middle' }
                        label: { text: "" , position: 'outer-middle' }
                    } 
            }, 
        });
        $('#c3-qc-chart-' + varList[i]).prepend("<h3 style='text-align:center'>" + varLabel[i] + "</h3>");


	}

}

CCF.lsreports.doDownload = function() {
	var csv = CCF.lsreports.generateCSV();
	var data = new Blob([csv]);
	var ele = document.getElementById("qc-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.lsreports.generateCSV = function() {

    var dta = CCF.lsreports.qcDataTableData;

    var keys = Object.keys(dta[0]);

    var result = keys.join(",") + "\n";

    dta.forEach(function(obj){
        keys.forEach(function(k, ix){
            if (typeof obj[k] === 'undefined') {
                obj[k] = "";
            }
            if (ix) result += ",";
            result += ((obj[k].toString().indexOf(",")<0 && obj[k].toString().indexOf(" ")<0) ? obj[k].toString().replace(/\r?\n/g,'') : "\"" + obj[k].toString().replace(/\r?\n/g,'').replace('"','\\"') + "\"");

        });
        result += "\n";
    });

    return result;

}


