
//# sourceURL=scanQualityReports/sqReports.js

if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.sqreports === 'undefined') {
	CCF.sqreports = { 
		//populated : false
	};
}

if (typeof CCF.sqreports.project === 'undefined' || CCF.sqreports.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.sqreports.project = queryParams.get("project");
}

CCF.sqreports.initSummary = function(project) {
	//console.log("INITIALIZE SUMMARY REPORT");
 
  $('#sqSummaryReport-contents').html("<div id='sqs-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='sqSummaryReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>");


  XNAT.xhr.getJSON({
     url: '/xapi/scanQualityReports/project/' + CCF.sqreports.project +
          '/summaryTables',
     success: function(data) {
	CCF.sqreports.summaryDatas = data;
	for (var key in data) {
		if (data.hasOwnProperty(key)) {
        		$('#sqSummaryReport-table').append("<h2>" + key + "</h2");
			var dataTable = data[key];
			var columnVar = CCF.sqreports.buildColumnsFromTable(dataTable);
		        XNAT.table.dataTable(dataTable, {
		           table: {
		              id: 'ls-sq-summarytable',
		              className: 'ls-sq-table'
		           },
		           width: 'fit-content',
		           overflowY: 'hidden',
		           overflowX: 'hidden',
		           columns: columnVar
		        }).render($('#sqSummaryReport-table'))
        		//$("#sqSummaryReport-table").append("<em>The data used to generate this report was last updated at " + (new Date(CCF.sqreports.dataLastUpdateTime).toLocaleString()) +"</em>");
		}
	}
      	$('#sqSummaryReport-table').css("text-align","center");
        $("#sqSummaryReport-table").find(".data-table-wrapper").css({ "display":"table", "margin":"auto"});
     	$("#sqs-building-div").remove();
     },
     failure: function(data) {
     	$("#sqs-building-div").html("<h3>Failed to build report</h3>");
     }
  });


}

CCF.sqreports.buildColumnsFromTable = function(dataTable) {
	var columnVar = {};
	var data0 = dataTable[0];
	for (var col in data0) {
		if (data0.hasOwnProperty(col)) {
              		columnVar[col] = {
		                label: col,
		            	td: {'className': 'center'},
		                sort: false
              		};
		}
	}
	return columnVar;
}

CCF.sqreports.initDataTable = function(project) {
	//console.log("INITIALIZE DATA TABLE");

	$("a[href='#sq-data-tab']").click(function() {
		setTimeout(function(){
			CCF.sqreports.populateRowCount();
		},10);
	});

  $('#sqDataReport-contents').html("<div id='sqd-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='sqDataReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='sqreports-row-count-container''></div>");

  var columnVar = 
           {
              subject: {
                label: 'Subject',
                filter: true,
            	td: {'className': 'subject center rowcount'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.sqreports.project + "/subjects/" + this.subjId + "' target='_blank'>" + this.subjLabel + "</a>";
                },
                sort: true
              },
              session: {
                label: 'Session',
                filter: true,
            	td: {'className': 'session center'},
                apply: function() {
                  return "<a href='/data/projects/" + CCF.sqreports.project + "/subjects/" + this.subjId + "/experiments/" + this.expId +"' target='_blank'>" + this.expLabel + "</a>";
                },
                sort: true
              },
              scanId: {
                label: 'ScanId',
                filter: true,
            	td: {'className': 'scanId center sq-highlight'},
                sort: true
              },
              scanType: {
                label: 'ScanType',
                filter: true,
            	td: {'className': 'scanType center sq-highlight'},
                sort: true
              },
              scanDesc: {
                label: 'ScanDesc',
                filter: true,
            	td: {'className': 'scanDesc center sq-highlight'},
                sort: true
              },
              scanQuality: {
                label: 'ScanQuality',
                filter: true,
            	td: {'className': 'scanQuality center'},
                sort: true
              },
              scanRating: {
                label: 'ScanRating',
                filter: true,
            	td: {'className': 'scanRating center'},
                sort: true
              },
              scanCount: {
                label: 'ScanCount',
                filter: true,
            	td: {'className': 'scanCount center'},
                sort: true
              },
              typeCount: {
                label: 'TypeCount',
                filter: true,
            	td: {'className': 'typeCount center'},
                sort: true
              },
              typeHighestRated: {
                label: 'HighestRated',
                filter: true,
            	td: {'className': 'typeHighestRated center'},
                sort: true
              },
              scanNote: {
                label: 'ScanNote',
                filter: true,
            	td: {'className': 'scanNote center', 'style': 'min-width:280px;max-width:560px'},
                sort: true
              },
              expScanner: {
                label: 'Scanner',
                filter: true,
            	td: {'className': 'expScanner center'},
                sort: true
              },
              expSite: {
                label: 'Site',
                filter: true,
            	td: {'className': 'expSite center'},
                sort: true
              }
           }
  ;

// set up custom filter menus
function filterMenuElement(prop){
  //console.log(prop);
  if (!prop) return false;
  // call this function in context of the table
  var $pipelineTable = $(this);
  //console.log($pipelineTable);
  var FILTERCLASS = 'filter-' + prop;
  //console.log(FILTERCLASS);
  return {
    id: 'pcp-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        $("td." + prop).each(function() {
          if ($(this).hasClass("filter")) {
              return true;
          }
          var $row = $(this).closest('tr');
           //console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })

	CCF.sqreports.populateRowCount();

      }
    }
  };
}


/*
  XNAT.xhr.getJSON({
     url: '/xapi/lifespanReports/project/' + CCF.sqreports.project +
          '/lastCacheUpdate',
     success: function(data) {
        CCF.sqreports.dataLastUpdateTime = data;
*/

	CCF.sqreports.initSummary();

	  XNAT.xhr.getJSON({
	     url: '/xapi/scanQualityReports/project/' + CCF.sqreports.project +
	          '/dataTable',
	     success: function(data) {
	        CCF.sqreports.sqDataTableData = data;

	        XNAT.table.dataTable(CCF.sqreports.sqDataTableData, {
	           table: {
	              id: 'ls-sq-datatable',
	              className: 'ls-sq-table sq-highlight'
	           },
	           width: 'fit-content',
	           overflowY: 'hidden',
	           overflowX: 'hidden',
	           columns: columnVar
	        }).render($('#sqDataReport-table'))
	        $("#sqDataReport-table").find(".data-table-wrapper").css("display","table");
	     	$("#sqd-building-div").html("<div style='width:100%;text-align:right'><a id='sq-download-csv' download='ScanQualityDataTableReport.csv' onclick='CCF.sqreports.doDownload()' type='text/csv'>Download CSV</a></div>");
	        setTimeout(function(){
	        	CCF.sqreports.populateRowCount();
	        },50);
	        $('input.filter-data').keyup(function() {
	            setTimeout(function(){
	               CCF.sqreports.populateRowCount();
	           },50)
	        });
	        setTimeout(function(){
	    		$('.sq-highlight').on("click",function() {
				$(this).closest('tr').find('td').each(function(){
					if ($(this).css("background-color").toString().indexOf('238')>=0) {
						$(this).css("background-color","");
					} else {
						$(this).css("background-color","rgb(238,238,238)");
					}
				});
			});
	        },100);
  		$("li.tab[data-tab='qc-summary-tab']").click();
	
	     },
	     failure: function(data) {
	     	$("#sqd-building-div").html("<h3>Failed to build report</h3>");
	     }
	  });

/*

     },
     failure: function(data) {
     	$("#sqd-building-div").html("<h3>Failed to build report</h3>");
     }
  });
*/

}


CCF.sqreports.populateRowCount = function() {
    if ($("#sqd-building-div").find('img').length<1) {
    	$('#sqreports-row-count-container').html("<em>" + ($('.rowcount').closest('tr').filter(":visible")).length + " rows match query filters.  " +
		"NOTE: Click on any non-link row to highlight that row for viewing.<br>" +
		"</em>");
		/*
		"The data used to generate this report was last updated at " + (new Date(CCF.sqreports.dataLastUpdateTime).toLocaleString()) +"</em>");
		*/
    } else {
    	$('#sqreports-row-count-container').empty();
    }
}


CCF.sqreports.doDownload = function() {
	var csv = CCF.sqreports.generateCSV();
	var data = new Blob([csv]);
	var ele = document.getElementById("sq-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.sqreports.generateCSV = function() {

    var dta = CCF.sqreports.sqDataTableData;

    var keys = Object.keys(dta[0]);

    var result = keys.join(",") + "\n";

    dta.forEach(function(obj){
        keys.forEach(function(k, ix){
            if (typeof obj[k] === 'undefined') {
                obj[k] = "";
            }
            if (ix) result += ",";
            result += ((obj[k].toString().indexOf(",")<0 && obj[k].toString().indexOf(" ")<0) ? obj[k].toString().replace(/\r?\n/g,'') : "\"" + obj[k].toString().replace(/\r?\n/g,'').replace('"','\\"') + "\"");
        });
        result += "\n";
    });

    return result;

}


