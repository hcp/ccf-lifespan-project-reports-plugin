(function() {
    var $;

    $ = jQuery;

    $.pivotUtilities.c3_renderers = {
        Scatterplot: function(pivotData, opts) {
            //result = $("<div id='c3chart' name='c3chart' class='c3' style='width: 100%; height: 100%;'>");
            //result = $("<div>"); 
            if ((pivotData.colAttrs==undefined || pivotData.colAttrs.length!=1) || (pivotData.rowAttrs==undefined || pivotData.rowAttrs.length!=1)) {
                $(".pvtRendererArea").html("<div id='c3chart' style='width: 700px; height: 500px;'></div>");
                $("#c3chart").html("<h4>Scatterplot charts currently support only charts with a single variable on each axis</h4>");
                result = $("#c3chart")[0];
                return result;
            }
            $(".pvtRendererArea").html("<div id='c3chart'><div style='width:100%;height:50px;text-align:left;margin-left:20px;'>" +
                                         "<h4 style='font-size:14px'>" + pivotData.rowAttrs[0] + " by " + pivotData.colAttrs[0] + " <span id='c3pearson'></span></h4></div>" + 
                                         "<div id='c3svg' style='width: 700px; height: 450px;'></div></div>");
            result = $("#c3chart")[0];
            var svgdiv = $("#c3svg")[0];

            var filterResults = $('#search_tabs').data("filterresults");

            var colData = [];
            var rowData = [];

            for (var rowval in pivotData.tree) {
                var rowobj=pivotData.tree[rowval];
                for (var colval in rowobj) {
                    var colobj=rowobj[colval];
                    if (colobj["count"]!=undefined) {
                        for (var i=1;i<=colobj["count"];i++) {
                            try {
                                var colarrv = colval;
                                var rowarrv = rowval
                                if (colarrv!=undefined && String(colarrv).toLowerCase()!="null" &&
                                    rowarrv!=undefined && String(rowarrv).toLowerCase()!="null") {
                                    colData.push(colarrv);
                                    rowData.push(rowarrv);
                                }
                            } catch(e) {
                                console.log(e);
                            }
                        }
                    } else {
                        $(".pvtAggregator").find("option").filter(function() {
                                return $(this).text()=='Count';
                            }).prop('selected',true).trigger('change');
                        $(".pvtRendererArea").html("<div id='c3chart' style='width: 700px; height: 500px;'></div>");
                        result = $("#c3chart")[0];
                        return result;
                    }   
                }
            }

            var c_xs = {};
            c_xs[pivotData.rowAttrs[0]] = pivotData.colAttrs[0];
            var c_data = [];
    
            // create clone of stored array, convert values if necessary
            var colDataCl = colData.slice(0);
            var rowDataCl = rowData.slice(0);
    
            // Convert array with string values if necessary keeping values array for ticks
            var colKeys = getKeyValues(pivotData.getColKeys());
            var rowKeys = getKeyValues(pivotData.getRowKeys());
            var plotValues = [];
            var x_axis={show:true};
            try {
                if (needConvert(colDataCl)) {
                    convertArray(colDataCl,colKeys);
                    x_axis["tick"]={
                                format: function(x) { 
                                        return (pivotData.getColKeys()[x]!=undefined) ? pivotData.getColKeys()[x][0] : '';
                                                    },
                                count: pivotData.getColKeys().length,
                            };
                } else {
                    convertArrayToNumeric(colDataCl);
                }
            } catch(e) {
            }
            x_axis["label"]=pivotData.colAttrs[0];
            var y_axis={show:true};
            try {
                if (needConvert(rowDataCl)) {
                    convertArray(rowDataCl,rowKeys);
                    // NOTE:  C3 currently seems to have a bug such that the y-axis tick count does not take affect
                    y_axis["tick"]={
                                format: function(x) { 
                                        return (pivotData.getRowKeys()[x]!=undefined) ? pivotData.getRowKeys()[x][0] : '';
                                                    },
                                count: pivotData.getRowKeys().length,
                            };
                } else {
                    convertArrayToNumeric(rowDataCl);
                }
            } catch(e) {
            }
            y_axis["label"]=pivotData.rowAttrs[0];

            var regLine = findLineByLeastSquares(colDataCl,rowDataCl);
            var pearson = getCorrelationCoeffizient(colDataCl,rowDataCl);

            if (!isNaN(pearson)) {
                $("#c3pearson").html("&nbsp;(&rho;=" + pearson + ")");
            }

            var regDataX = regLine[0];
            var regDataY = regLine[1];
            var typeobj = {};
            typeobj[pivotData.rowAttrs[0]] = "scatter";
            typeobj["regDataY"] = "line";
            regDataX.unshift("regDataX");
            regDataY.unshift("regDataY");
    
            colDataCl.unshift(pivotData.colAttrs[0]);
            rowDataCl.unshift(pivotData.rowAttrs[0]);
            c_data.push(colDataCl);
            c_data.push(rowDataCl);
            if ($.inArray(NaN,regDataX)<0 && $.inArray(NaN,regDataY)<0) {
                c_xs["regDataY"] = "regDataX";
                c_data.push(regDataX);
                c_data.push(regDataY);
            }
            var chart = c3.generate({
                bindto: svgdiv,
                data: {
                        xs: c_xs,
                        columns: c_data,
                        types: typeobj,
                },
                point: {
                        show: true,
                        focus: {
                            expand: {
                                enabled: false
                            },
                        },
                        select: {
                             r: function(x) {
                                if (x.id=="regDataY") {
                                   return 0;
                                } else {
                                   return 4;
                                }
                             } 
                         },
                         r: function(x) {
                            if (x.id=="regDataY") {
                                return 0;
                            } else {
                                return 4;
                            }
                         } 
                },
                legend: {
                        show: false 
                },
                tooltip: {
                        show: false 
                },
                axis: {
                    x: x_axis,
                    y: y_axis
                }, 
            });

      function getKeyValues(pivotKeys) {
            var returnArr = [];
            for (var i=0;i<pivotKeys.length;i++) {
                returnArr.push(pivotKeys[i][0]);
            }
            return returnArr;
      }

      function getKeyPos(arr,val) {
          for (var i=0;i<arr.length;i++) {
              if (arr[i][0]==val) {
                  return i;
              }
          }
          return -1;
      }
      
      function needConvert(arr) {
          var arraylen = arr.length;
          var countNaN = 0
          for (var i=0;i<arraylen;i++) {
              if (arr[i]!=null && arr[i]!=undefined && isNaN(arr[i])) {
                  countNaN++;
                  if (countNaN>(arraylen/10)) {
                      return true;
                  }
              }
          }
          return false;
      }
      
      function convertArray(arr,valArr) {
          for (var i=0;i<arr.length;i++) {
              if ($.inArray(arr[i],valArr)<0) {
                  valArr.push(arr[i]);
              }
              arr[i]=$.inArray(arr[i],valArr);
          }
      }

      function convertArrayToNumeric(arr) {
          for (var i=0;i<arr.length;i++) {
              arr[i]=Number(arr[i]);
          }
      }

// Function from http://dracoblue.net/dev/linear-least-squares-in-javascript/
function findLineByLeastSquares(values_x, values_y) {
    var sum_x = 0;
    var sum_y = 0;
    var sum_xy = 0;
    var sum_xx = 0;
    var count = 0;

    /*
     * We'll use those variables for faster read/write access.
     */
    var x = 0;
    var y = 0;
    var values_length = values_x.length;

    if (values_length != values_y.length) {
        throw new Error('The parameters values_x and values_y need to have same size!');
    }

    /*
     * Nothing to do.
     */
    if (values_length === 0) {
        return [ [], [] ];
    }

    /*
     * Calculate the sum for each of the parts necessary.
     */
    for (var v = 0; v<values_length; v++) {
        x = values_x[v];
        y = values_y[v];
        sum_x += x;
        sum_y += y;
        sum_xx += x*x;
        sum_xy += x*y;
        count++;
    }

    /*
     * Calculate m and b for the formular:
     * y = x * m + b
     */
    var m = (count*sum_xy - sum_x*sum_y) / (count*sum_xx - sum_x*sum_x);
    var b = (sum_y/count) - (m*sum_x)/count;

    /*
     * We will make the x and y result line now
     */
    var result_values_x = [];
    var result_values_y = [];

    for (var v = 0; v<values_length; v++) {
        x = values_x[v];
        y = x * m + b;
        result_values_x.push(x);
        result_values_y.push(y);
    }

    return [result_values_x, result_values_y];
}

  // MIT License 
  // by @mrflix
  // https://gist.github.com/mrflix/6538300
  function getCorrelationCoeffizient(data1, data2){
    var sumI = 0, sumI2 = 0, sumIR = 0, sumR = 0, sumR2 = 0;
    var nR = data1.length;
 
    for(var i=0; i<nR; i++){
      var vI = data1[i];
      var vR = data2[i];
 
      sumI += vI;
      sumI2 += vI * vI;
 
      sumR += vR;
      sumR2 += vR * vR;
 
      sumIR += vI * vR;
    }
 
    var meanI = sumI / nR;
    var meanR = sumR / nR;
    var sigmaI = Math.sqrt(sumI2 - nR * meanI * meanI);
    var sigmaR = Math.sqrt(sumR2 - nR * meanR * meanR);
    
    return ((sumIR - nR * meanI * meanR) / (sigmaI * sigmaR)).toFixed(4);
  }

      return result;
    }
  }
}).call(this);

